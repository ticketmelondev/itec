import React, { useEffect, useState } from 'react'
import { getDayLists, getMonthLists, getYearLists } from '@util/dataFormate'
import moment from 'moment'

import Grid from '@material-ui/core/Grid'

import Autocomplete from '@components/Autocomplete'

function CustomSelectDate({ label, options, value, handleChange }) {
  return (
    <Autocomplete label={label} options={options} value={value} onChange={handleChange} outlined />
  )
}

function SelectDob({ onChange, value, error, errorText, ...rest }) {
  const [date, setDate] = useState({
    label: value ? value.split('/')[0] : '',
    value: value ? value.split('/')[0] : '',
  })
  const [month, setMonth] = useState({
    label: value ? monthList[value.split('/')[1]] : '',
    value: value ? value.split('/')[1] : '',
  })
  const [year, setYear] = useState({
    label: value ? value.split('/')[2] : '',
    value: value ? value.split('/')[2] : '',
  })
  const [invalid, setInvalid] = useState(false)

  // console.log('SelectDob', {
  //   value,
  //   date,
  //   month,
  //   year,
  //   monthList,
  //   ...rest,
  // })
  useEffect(() => {
    // console.log('useEffect')
    if (date.value && month.value && year.value) {
      const stringDate = `${date.value}/${month.value}/${year.value}`
      const isValidDate = moment(stringDate, 'DD/MM/YYYY').isValid()
      if (isValidDate) {
        onChange(stringDate)
        setInvalid(false)
      } else {
        setInvalid(true)
      }
      // console.log('useEffect', stringDate, isValidDate)
    }
  }, [date, month, year])

  return (
    <Grid container>
      <Grid item xs={4}>
        <div style={{ paddingRight: 5 }}>
          <CustomSelectDate
            label="DD"
            options={getDayLists(month.value, year.value)}
            value={date}
            handleChange={setDate}
          />
        </div>
      </Grid>
      <Grid item xs={4}>
        <div style={{ paddingRight: 5, paddingLeft: 5 }}>
          <CustomSelectDate
            label="MM"
            options={getMonthLists()}
            value={month}
            handleChange={setMonth}
          />
        </div>
      </Grid>
      <Grid item xs={4}>
        <div style={{ paddingLeft: 5 }}>
          <CustomSelectDate
            label="YYYY"
            options={getYearLists()}
            value={year}
            handleChange={setYear}
          />
        </div>
      </Grid>
      {error && <span style={{ color: '#ED1B24' }}>{errorText}</span>}
      {invalid && <span style={{ color: '#ED1B24' }}>invalid date</span>}
    </Grid>
  )
}

const monthList = {
  '01': 'January',
  '02': 'February',
  '03': 'March',
  '04': 'April',
  '05': 'May',
  '06': 'June',
  '07': 'July',
  '08': 'August',
  '09': 'September',
  '10': 'October',
  '11': 'November',
  '12': 'December',
}

export default SelectDob
