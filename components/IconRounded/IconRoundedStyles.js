import { makeStyles } from '@material-ui/core/styles'

const IconRoundedStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
  },
  titleTypography: {
    fontWeight: 600,
    fontSize: 14,
    lineHeight: '140%',
    color: theme.palette.primary.main,
  },
  avatarRounded: {
    color: '#fff',
    backgroundColor: theme.palette.primary.main,
    width: 28,
    height: 28,
    marginRight: 12,
  },
}))

export default IconRoundedStyles
