import React from 'react'
import MuiSnackbar from '@material-ui/core/Snackbar'
import MuiAlert from '@material-ui/lab/Alert'
import Slide from '@material-ui/core/Slide'

import Icon from '@components/Icon'

import SnackbarStyles from './SnackbarStyles'

export function Alert(props) {
  return (
    <MuiAlert
      iconMapping={{ error: <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>alert</Icon> }}
      elevation={6}
      variant="filled"
      {...props}
    />
  )
}

function TransitionLeft(props) {
  return <Slide {...props} direction="left" />
}

function Snackbar({ open, onClose, type, text }) {
  const classes = SnackbarStyles()

  return (
    <MuiSnackbar
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={open}
      autoHideDuration={6000}
      onClose={onClose}
      TransitionComponent={TransitionLeft}
    >
      <div>
        <Alert classes={{ filledError: classes.filledError }} severity={type} onClose={onClose}>
          {text}
        </Alert>
      </div>
    </MuiSnackbar>
  )
}

Snackbar.defaultProps = {
  text: 'This is an error message!',
}

export default Snackbar
