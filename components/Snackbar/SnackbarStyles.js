import { makeStyles } from '@material-ui/core/styles'

const SnackbarStyles = makeStyles((theme) => ({
  root: {},
  filledError: {
    backgroundColor: theme.palette.primary.main,
  },
}))

export default SnackbarStyles
