import { makeStyles } from '@material-ui/core/styles'

const BreadcrumbStyles = makeStyles((theme) => ({
  root: {},
  typographyLink: {
    cursor: 'pointer',
    color: '#999999',
  },
  typographyUnlink: {
    color: 'rgba(0, 0, 0, 0.56)',
  },
}))

export default BreadcrumbStyles
