import React from 'react'
import Breadcrumbs from '@material-ui/core/Breadcrumbs'
// import Link from '@material-ui/core/Link'
import NavigateNextIcon from '@material-ui/icons/NavigateNext'
import Typography from '@material-ui/core/Typography'
import BreadcrumbStyled from './BreadcrumbStyled'

import Link from '@components/Link'

function Breadcrumb({ routes }) {
  const classes = BreadcrumbStyled()

  return (
    <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />} aria-label="breadcrumb">
      {routes.map((route) =>
        !route.isActive ? (
          <Link route={route.page} passHref>
            <Typography className={classes.typographyLink}>{route.name}</Typography>
          </Link>
        ) : (
          <Typography className={classes.typographyUnlink}>{route.name}</Typography>
        ),
      )}
    </Breadcrumbs>
  )
}

Breadcrumb.defaultProps = {
  routes: [
    {
      name: 'Breadcrumb',
      page: 'home',
      isActive: true,
    },
  ],
}

export default Breadcrumb
