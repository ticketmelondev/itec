import React from 'react'
import FormControl from '@material-ui/core/FormControl'
import IconButton from '@material-ui/core/IconButton'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import ArrowForwardIcon from '@material-ui/icons/ArrowForward'
import InputAdornment from '@material-ui/core/InputAdornment'
import CalendarTodayIcon from '@material-ui/icons/CalendarToday'

import Datepicker from '@components/Datepicker'
import Input from '@components/Input'

import moment from 'moment'

const InputCustom = ({ onClick, value, errors, fullWidth, ...rest }) => {
  return (
    <Input
      placeholder="D MMM YYYY"
      endAdornment={
        <InputAdornment position="end">
          <IconButton style={{ color: '#cccccc', padding: 0, marginBottom: 8, padding: 5 }}>
            <CalendarTodayIcon />
          </IconButton>
        </InputAdornment>
      }
      // error={errors ? true : false}
      // helperText={errors ? errors.message : ' '}
      {...rest}
      onClick={onClick}
      value={value && moment(value).format('D MMM YYYY')}
      fullWidth={fullWidth}
    />
  )
}

function InputDate({ inputRef, name, ...rest }) {
  return (
    <Datepicker name={name} customInput={<InputCustom inputRef={inputRef} {...rest} />} {...rest} />
  )
}

export default InputDate
