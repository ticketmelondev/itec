import React, { useState } from 'react'
import DatePicker from 'react-datepicker'

import FormControl from '@material-ui/core/FormControl'
import IconButton from '@material-ui/core/IconButton'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import ArrowForwardIcon from '@material-ui/icons/ArrowForward'

import DatepickerStyles from './DatepickerStyles'

import moment from 'moment'

function range(from, to) {
  return Array.from({ length: to - from + 1 }, (_v, i) => i + from)
}
const years = range(1975, moment().add(5, 'y').get('year'))
// const years = moment().year()
const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
]

function Datepicker({ customInput, onChange, value, fullWidth, ...rest }) {
  const [startDate, setStartDate] = useState(null)
  const handleChange = (date) => {
    onChange(moment(date).format('D MMM YYYY'))
  }
  const classes = DatepickerStyles()

  return (
    <FormControl className={classes.formControl} fullWidth={fullWidth}>
      <div className={classes.root}>
        <DatePicker
          {...rest}
          selected={value && new Date(moment(value, 'D MMM YYYY'))}
          onChange={handleChange}
          customInput={customInput}
          renderCustomHeader={({
            date,
            changeYear,
            changeMonth,
            decreaseMonth,
            increaseMonth,
            prevMonthButtonDisabled,
          }) => {
            return (
              <div className={classes.headerRoot}>
                <div>
                  <IconButton className={classes.headerButton} size="small" onClick={decreaseMonth}>
                    {/* <Icons fontSize={12}>back</Icons> */}
                    <ArrowBackIcon />
                  </IconButton>
                </div>
                <div style={{ textAlign: 'center' }} className="react-datepicker__current-month">
                  <select
                    className={classes.customSelect}
                    value={months[moment(date).month()]}
                    onChange={({ target: { value } }) => changeMonth(months.indexOf(value))}
                  >
                    {months.map((option) => (
                      <option key={option} value={option}>
                        {option}
                      </option>
                    ))}
                  </select>
                  <select
                    className={classes.customSelect}
                    value={moment(date).year()}
                    onChange={({ target: { value } }) => changeYear(value)}
                  >
                    {years.map((option) => (
                      <option key={option} value={option}>
                        {option}
                      </option>
                    ))}
                  </select>
                </div>
                <div>
                  <IconButton className={classes.headerButton} size="small" onClick={increaseMonth}>
                    {/* <Icons fontSize={12}>more</Icons> */}
                    <ArrowForwardIcon />
                  </IconButton>
                </div>
              </div>
            )
          }}
        />
      </div>
    </FormControl>
  )
}

export default Datepicker
