export const PAGE_LANGUAGE = {
  TH: {},
  EN: {},
}

export const getAdminUser = (type) => {
  if (!type) return false
  // console.log('getAdminUser', type.indexOf('TEACHER'))
  if (type.indexOf('TEACHER') > -1) return true
  else return false
}

export const getStudentUser = (type) => {
  if (!type) return false
  // console.log('getStudentUser', type.indexOf('STUDENT'))
  if (type.indexOf('STUDENT') > -1) return true
  else return false
}

export const getUserAccessPromoCode = (email) => {
  if (
    email === 'por@riseaccel.com' ||
    email === 'rada@wonderfruit.co' ||
    email === 'jon.lor@gmail.com' ||
    email === 'wan@wonderfruit.co' ||
    email === 'larinda@jaithepfestival.com' ||
    email === 'marketing@fungjai.com' ||
    email === 'karmakliq@gmail.com' ||
    email === 'alisa@wonderfruitfestival.com' ||
    email === 'p@homeaway-agency.com'
  )
    return true
  else return false
}

export const VERSION_2C2P = '8.5'
export const MERCHANT_ID = '764764000003261'
export const CURRENCY = '764'

export const AUTH_COOKIE_NAME = 'itec-token'
export const AUTH_COOKIE_MAX_AGE = 60 * 60 * 24 // 24 hour
export const AUTH_COOKIE_ONE_YEAR_AGE = 24 * 60 * 60 * 365 // 1 year
