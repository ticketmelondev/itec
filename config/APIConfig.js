const API_HOST_PRODUCTION = ''
const API_HOST_TEST = 'https://o9n8z5l1lj.execute-api.ap-southeast-1.amazonaws.com/dev'
const APPLICATION_MODE = process.env.NODE_ENV
let API_HOST = ''

const API_HOST_2C2P_DEV = 'https://demo2.2c2p.com/2C2PFrontEnd/RedirectV3/payment'
const API_HOST_2C2P = 'https://t.2c2p.com/RedirectV3/payment'

const API_HOST_CALLBACK_2C2P_DEV = 'http://localhost:3000/2C2P-response'
const API_HOST_CALLBACK_2C2P = 'https://www.eduworld.ac.th/2C2P-response'

let PAYMENT_URL_2C2P = ''
let RESULT_PAYMENT = ''

if (APPLICATION_MODE === 'production') {
  API_HOST = API_HOST_TEST
  PAYMENT_URL_2C2P = API_HOST_2C2P
  RESULT_PAYMENT = API_HOST_CALLBACK_2C2P
} else {
  API_HOST = API_HOST_TEST
  PAYMENT_URL_2C2P = API_HOST_2C2P
  RESULT_PAYMENT = API_HOST_CALLBACK_2C2P
}

const API_ROUTER = {
  ACCOUNT: {
    url: 'account',
  },
  ACCOUNT_ID: {
    url: 'account/param',
  },
  AUTH: {
    url: 'auth',
  },
  ACCOUNT_FORGOT: {
    url: 'account/forgot',
  },
  SERVICE_EMAIL: {
    url: 'services/email_checker',
  },
  GET_ALL_COURSES: {
    url: 'courses',
  },
  GET_COURSE_DETAILS: {
    url: 'course/param',
  },
  SERVICE_CHECK_TECHER_SCHDULE: {
    url: 'services/checkschedule',
  },
  CREATE_ORDER: {
    url: 'order',
  },
  GET_ORDER: {
    url: 'order/param',
  },
  GET_STUDENT: {
    url: 'order/student',
  },
  GET_TEACHER: {
    url: 'order/teacher',
  },
  GET_LEVEL: {
    url: 'course/level/param',
  },
  GET_HASH: {
    url: 'payment/gethash',
  },
  CREATE_SESSION: {
    url: 'order/param/sessions',
  },
  UPDATE_SESSION: {
    url: 'order/param1/sessions/param2',
  },
  GET_INSTITUTE: {
    url: 'institute',
  },
  DELETE_SESSION: {
    url: 'order/param1/sessions',
  },
}

export const getAPIRouter = (api) => {
  let result = ''
  switch (api) {
    case 'PAYMENT_URL_2C2P': {
      result = PAYMENT_URL_2C2P
      break
    }
    case 'RESULT_PAYMENT': {
      result = RESULT_PAYMENT
      break
    }
    case 'ACCOUNT': {
      const ROUTER = API_ROUTER.ACCOUNT
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'ACCOUNT_ID': {
      const ROUTER = API_ROUTER.ACCOUNT_ID
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'AUTH': {
      const ROUTER = API_ROUTER.AUTH
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'ACCOUNT_FORGOT': {
      const ROUTER = API_ROUTER.ACCOUNT_FORGOT
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'SERVICE_EMAIL': {
      const ROUTER = API_ROUTER.SERVICE_EMAIL
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'GET_ALL_COURSES': {
      const ROUTER = API_ROUTER.GET_ALL_COURSES
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'GET_COURSE_DETAILS': {
      const ROUTER = API_ROUTER.GET_COURSE_DETAILS
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'SERVICE_CHECK_TECHER_SCHDULE': {
      const ROUTER = API_ROUTER.SERVICE_CHECK_TECHER_SCHDULE
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'CREATE_ORDER': {
      const ROUTER = API_ROUTER.CREATE_ORDER
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'GET_ORDER': {
      const ROUTER = API_ROUTER.GET_ORDER
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'GET_STUDENT': {
      const ROUTER = API_ROUTER.GET_STUDENT
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'GET_TEACHER': {
      const ROUTER = API_ROUTER.GET_TEACHER
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'GET_LEVEL': {
      const ROUTER = API_ROUTER.GET_LEVEL
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'GET_HASH': {
      const ROUTER = API_ROUTER.GET_HASH
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'CREATE_SESSION': {
      const ROUTER = API_ROUTER.CREATE_SESSION
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'UPDATE_SESSION': {
      const ROUTER = API_ROUTER.UPDATE_SESSION
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'GET_INSTITUTE': {
      const ROUTER = API_ROUTER.GET_INSTITUTE
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    case 'DELETE_SESSION': {
      const ROUTER = API_ROUTER.DELETE_SESSION
      const url = ROUTER.url
      result = `${API_HOST}/${url}`
      break
    }
    default: {
      result = ''
    }
  }
  return result
}
