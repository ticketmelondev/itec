import moment from 'moment'
import { range } from 'lodash'

export const bookingType = [
  { label: 'Once', value: 'once' },
  { label: 'Daily', value: 'daily' },
  { label: 'Weekly', value: 'weekly' },
]

export const dayOfClass = [
  { label: 'Monday', value: 1 },
  { label: 'Tuesday', value: 2 },
  { label: 'Wednesday', value: 3 },
  { label: 'Thursday', value: 4 },
  { label: 'Friday', value: 5 },
  { label: 'Saturday', value: 6 },
  { label: 'Sunday', value: 0 },
]

export const startDate = (day) => {
  let output = []
  let weeks = 7
  const now = moment().day()
  if (now <= day) weeks = 0

  for (let i = 0; i < 15; i++) {
    const dayOfWeek = moment()
      .day(day + weeks)
      .format('D MMM YYYY')
    output = [...output, dayOfWeek]
    weeks = weeks + 7
  }
  // console.log('startDate', { output, now: moment().day() })
  return output
}

export const timePeriod = (data, { value }) => {
  const convertDate = moment(value, 'D MMM YYYY').format('YYYY-MM-DD')
  const values = data[convertDate]
  // console.log('timePeriod', data, { value, values })
  const res = values.FREE_TIME.split('+').map((item) => item.replace(/[()]/gi, ''))
  const dataSet = res.map((i) => {
    const str = i.split('-')
    // console.log('str', str)
    return `${moment(str[0], 'HH:mm').format('HH:mm')} - ${moment(str[1], 'HH:mm').format('HH:mm')}`
  })
  // console.log('timePeriod', { res, dataSet, data, value, values, convertDate })
  return dataSet
}

export const toTimeSteamp = (type, date, class_time, session) => {
  let weeks = 0
  let output = []

  if (type === 'daily') {
    for (let i = 0; i < session; i++) {
      const nextDay = moment(date, 'D MMM YYYY').add(i, 'days')
      const dateTimeStr = `${moment(nextDay).format('D MMM YYYY')} ${class_time}`
      const dateTimeFormat = moment(dateTimeStr, 'D MMM YYYY HH:mm')
      const timeSteamp = moment(dateTimeFormat).valueOf()
      output = [...output, timeSteamp]
    }
    return output
  } else if (type === 'weekly') {
    for (let i = 0; i < session; i++) {
      const nextWeek = moment(date, 'D MMM YYYY').add(weeks, 'days')
      const dateTimeStr = `${moment(nextWeek).format('D MMM YYYY')} ${class_time}`
      const dateTimeFormat = moment(dateTimeStr, 'D MMM YYYY HH:mm')
      const timeSteamp = moment(dateTimeFormat).valueOf()
      output = [...output, timeSteamp]
      weeks = weeks + 7
    }
    return output
  } else {
    const dateTimeStr = `${date} ${class_time}`
    const dateTimeFormat = moment(dateTimeStr, 'D MMM YYYY HH:mm')
    const timeSteamp = moment(dateTimeFormat).valueOf()
    return (output = [timeSteamp])
  }
}

export const sessionNumber = (quantity = 0, session = 0) => {
  const range = quantity - session
  let output = []

  for (let i = 0; i < range; i++) {
    const label = i + 1
    const value = i + 1
    output = [...output, { label: label + '', value }]
  }

  // console.log('sessionNumber', { output })
  return output
}
