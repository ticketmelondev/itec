export const filterLevels = (data, id) => {
  const filterFunc = (data) => {
    return data.LEVEL_ID === id
  }
  return data.filter(filterFunc)
}

export const filterCourseId = (data, id) => {
  const filterFunc = (data) => {
    return data.DETAIL.COURSE_ID === id
  }
  return data.filter(filterFunc)
}

export const filterSessionCourseId = (data, id) => {
  const filterFunc = (data) => {
    return data.ORDER_ID === id
  }
  return data.filter(filterFunc)
}

export const finderLevelId = (data, id) => {
  const finderFunc = (data) => {
    return data.LEVEL_ID === id
  }
  return data.find(finderFunc)
}
