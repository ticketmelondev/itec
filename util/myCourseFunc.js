import {
  filterLevels,
  filterCourseId,
  filterSessionCourseId,
  finderLevelId,
} from '@util/filterData'
import { toDateTimeFormat, toDateTimeFormatCalendar, sortFunc } from '@util/dataFormate'
import moment from 'moment'

export const fomatOurCourse = (courses) => {
  const datas = courses
    .map((course) => {
      return {
        ...course,
        key: course.DETAIL.INSTITUTE + course.DETAIL.COURSE_NAME,
      }
    })
    .sort(sortFunc('key'))
  // console.log('fomatOurCourse', { courses, datas })
  return datas
}

export const formatMyCourse = (orders, courses) => {
  const sessions = orders.SESSIONS
  const structures = orders.DETAIL
  const formatStructure = structures.reduce((arr, structure) => {
    const itemCourse = filterCourseId(courses, structure.COURSE_ID)
    const itemSessions = filterSessionCourseId(sessions, structure.ORDER_ID)
    // console.log('itemSessions', itemSessions)
    const reducer = {
      ...structure,
      COURSE_DETAIL: itemCourse.length > 0 ? itemCourse[0] : {},
      SESSIONS: itemSessions.length > 0 ? itemSessions : [],
    }
    if (structure.STATUS === 'PAID') {
      arr['orderSuccess'] = [...(arr['orderSuccess'] || []), reducer]
    } else {
      arr['orderNotSuccess'] = [...(arr['orderNotSuccess'] || []), reducer]
    }
    return arr
  }, {})
  // console.log('formatStructure', formatStructure)
  return formatStructure
}

export const formatCalendar = async (courses) => {
  // console.log('formatCalendar', courses)
  let structures = []
  await courses.map((course) => {
    const active = course.SESSIONS.filter((i) => i.S_STATUS !== 'INACTIVE') || []
    // console.log('active', active)
    active.map((session) => {
      structures = [
        ...structures,
        {
          ...session,
          START_TIME_FORMATE: toDateTimeFormatCalendar(session.START_TIME),
          END_TIME_FORMATE: toDateTimeFormatCalendar(session.END_TIME),
          MY_COUSE: course,
        },
      ]
    })
  })

  const formatStructure = structures.reduce((arr, structure) => {
    arr[structure.START_TIME_FORMATE.date] = [
      ...(arr[structure.START_TIME_FORMATE.date] || []),
      structure,
    ]
    return arr
  }, {})
  // console.log('formatStructure', formatStructure)
  return formatStructure
}

export const formatCalendarTeacher = async (courses) => {
  // console.log('formatCalendar', courses)
  let structures = []
  await courses.map((course) => {
    const active =
      course.SESSIONS.filter(
        (i) => i.S_STATUS !== 'INACTIVE' && i.S_STATUS !== 'WAITING_APPROVE',
      ) || []
    // console.log('active', active)
    active.map((session) => {
      structures = [
        ...structures,
        {
          ...session,
          START_TIME_FORMATE: toDateTimeFormatCalendar(session.START_TIME),
          END_TIME_FORMATE: toDateTimeFormatCalendar(session.END_TIME),
          MY_COUSE: course,
        },
      ]
    })
  })

  const formatStructure = structures.reduce((arr, structure) => {
    arr[structure.START_TIME_FORMATE.date] = [
      ...(arr[structure.START_TIME_FORMATE.date] || []),
      structure,
    ]
    return arr
  }, {})
  // console.log('formatStructure', formatStructure)
  return formatStructure
}

export const expanSessionFormat = (sessions, levels) => {
  const active = sessions.filter((i) => i.S_STATUS !== 'INACTIVE') || []
  const items = active
    .map((session) => {
      const level = finderLevelId(levels, session.LEVEL_ID)
      return {
        date: session.START_TIME,
        text: session?.NOTE || '-',
        status: session.S_STATUS === 'COMPLETE' ? 'completed' : 'incomplete',
        sessionId: session.SESSION_ID,
        orderId: session.ORDER_ID,
        details: {
          ...session,
        },
      }
    })
    .sort((a, b) => {
      return a.date - b.date
    })
  // console.log('expanSessionFormat', { sessions, levels, items, active })
  return items
}

export const expanSessionFormatTeacher = (sessions, levels) => {
  const active =
    sessions.filter((i) => (i.S_STATUS !== 'INACTIVE') & (i.S_STATUS !== 'WAITING_APPROVE')) || []
  const items = active
    .map((session) => {
      const level = finderLevelId(levels, session.LEVEL_ID)
      return {
        date: session.START_TIME,
        text: session?.NOTE || '-',
        status: session.S_STATUS === 'COMPLETE' ? 'completed' : 'incomplete',
        sessionId: session.SESSION_ID,
        orderId: session.ORDER_ID,
        details: {
          ...session,
        },
      }
    })
    .sort((a, b) => {
      return a.date - b.date
    })
  // console.log('expanSessionFormatTeacher', { sessions, levels, items })
  return items
}

export const statusSessionFormat = (sessions, qty) => {
  let countCompleted = 0
  let countOngoing = 0
  // const quantity = sessions.length
  const quantity = qty
  sessions.map((session) => {
    if (session.S_STATUS === 'COMPLETE') {
      countCompleted = countCompleted + 1
    } else {
      countOngoing = countOngoing + 1
    }
  })
  // console.log('statusSessionFormat', { countCompleted, countOngoing })

  if (countCompleted === quantity) {
    return 'Completed'
  } else {
    return `On Going [${countCompleted} / ${quantity}]`
  }
}

export const rescheduleTimeStem = (date, class_time) => {
  const dateTimeStr = `${date} ${class_time}`
  const dateTimeFormat = moment(dateTimeStr, 'D MMM YYYY HH:mm')
  const timeSteamp = moment(dateTimeFormat).valueOf()
  return timeSteamp
}

const convertHourse2Seccond = (creatTicketTimeLimit) => {
  const hmSplit = creatTicketTimeLimit.split(':')
  const hours = hmSplit[0] === '00' ? 0 : hmSplit[0]
  const minutes = hmSplit[1] === '00' ? 0 : hmSplit[1]
  return parseInt(hours) * 60 * 60 + parseInt(minutes) * 60
}

export const bookandrescheduleflow = (sTime, eTime, creatTicketTime, creatTicketTimeLimit) => {
  const sTimeFormat = moment(sTime).format('D MMM YYYY HH:mm')
  const eTimeFormat = moment(eTime).format('D MMM YYYY HH:mm')
  const startTime = moment(sTimeFormat, 'D MMM YYYY HH:mm').valueOf()
  const expiredTime = moment(eTimeFormat, 'D MMM YYYY HH:mm').add(1, 'y').valueOf()
  const today = moment().set({ hour: 23, minute: 59 }).valueOf()
  const tomorrow = moment(today).add(86400, 's').valueOf()
  const negativeTime = moment(startTime).subtract(creatTicketTime, 's').valueOf()
  const startLimitTime = moment().set({ hour: 0, minute: 0, second: 0 }).valueOf()
  const limitTime = moment(startLimitTime)
    .add(convertHourse2Seccond(creatTicketTimeLimit), 's')
    .valueOf()
  const now = moment().valueOf()

  // console.log('bookandrescheduleflow', {
  //   startTime,
  //   expiredTime,
  //   creatTicketTime,
  //   today,
  //   tomorrow,
  //   negativeTime,
  //   creatTicketTimeLimit: limitTime,
  //   xxxxx: moment().add(limitTime, 's').valueOf(),
  //   now,
  //   startLimitTime,
  // })

  if (startTime > expiredTime) {
    console.log('1111')
    return false
  } else if (startTime < today) {
    console.log('2222')
    return false
  } else if (negativeTime < now) {
    console.log('3333')
    return false
  } else if (negativeTime > now) {
    console.log('4444')
    if (startTime > today && startTime < tomorrow) {
      console.log('4444 1111')
      if (now > limitTime) {
        console.log('4444 3333')
        return false
      } else {
        console.log('4444 4444')
        return true
      }
    } else {
      console.log('4444 2222')
      return true
    }
  }
}

export const fomatOurInstitute = (institute) => {
  return institute.sort(sortFunc('INDEX'))
}
