import moment from 'moment-timezone'
import numeral from 'numeral'

export const toNumber = (number, format = '0,0.00') => {
  return numeral(number).format(format)
}

export const getHours = () => {
  let hours = []
  for (var i = 0; i < 24; i++) {
    let number = preDigits(i, 2)
    hours.push({ label: number, value: number })
  }
  return hours
}

export const getMinutes = () => {
  let minutes = []
  for (var i = 0; i < 60; i += 5) {
    let number = preDigits(i, 2)
    minutes.push({ label: number, value: number })
  }
  return minutes
}

export const preDigits = (number, digits) => {
  return Array(Math.max(digits - String(number).length + 1, 0)).join(0) + number
}

export const toTimeSteamp = (
  dateTime = toDateTimeFormat(0, 'Asia/Bangkok'),
  timezone = 'Asia/Bangkok',
) => {
  const dateTimeStr = `${dateTime.date} ${dateTime.time ? dateTime.time.hour : '00'}:${
    dateTime.time ? dateTime.time.min : '00'
  }:00`
  // const dateTimeFormatConvert = moment.tz(dateTimeStr, 'DD/MM/YYYY HH:mm:ss')
  const dateTimeFormat = moment.tz(dateTimeStr, 'DD/MM/YYYY HH:mm:ss', timezone)
  const timeSteamp = moment(dateTimeFormat).valueOf()
  return timeSteamp
}

export const toDateTimeFormat = (timeSteamp, timezone = 'Asia/Bangkok') => {
  const dateTime = moment(timeSteamp)
  const output = {
    // date: dateTime.format('D MMM YYYY'),
    date: dateTime.format('ddd, MMM D'),
    time: {
      hour: dateTime.format('HH'),
      min: dateTime.format('mm'),
    },
  }
  return output
}

export const toDateTimeFormatCalendar = (timeSteamp, timezone = 'Asia/Bangkok') => {
  const dateTime = moment(timeSteamp)
  const output = {
    date: dateTime.format('D MMM YYYY'),
    time: {
      hour: dateTime.format('HH'),
      min: dateTime.format('mm'),
    },
  }
  return output
}

export const toTimeFormat = () => {
  const x = 5 //minutes interval
  let times = [] // time array
  let tt = 0 // start time

  //loop to increment the time and push results in array
  for (let i = 0; tt < 24 * 60; i++) {
    let hh = Math.floor(tt / 60) // getting hours of day in 0-24 format
    let mm = tt % 60 // getting minutes of the hour in 0-55 format
    // const time = ('0' + hh).slice(-2) + ':' + ('0' + mm).slice(-2)
    times[i] = ('0' + hh).slice(-2) + ':' + ('0' + mm).slice(-2)
    tt = tt + x
  }
  // console.log('times', times)
  return times
}

export const calculatAmount12Digit = (price) => {
  const amountFormat = price * 100
  const amount = toNumber(amountFormat, '000000000000')
  return amount
}

export const displayTime = (sessionTime) => {
  const h = Math.floor(sessionTime / 3600)
  const m = Math.floor((sessionTime % 3600) / 60)
  const s = Math.floor((sessionTime % 3600) % 60)
  // console.log('displayTime', { h, m, s })
  if (h > 0) return <span style={{ fontWeight: 'bold' }}>{h} hours</span>
  else if (m > 0) return <span style={{ fontWeight: 'bold' }}>{m} minute</span>
  return
}

export const sortFunc = (key, order = 'asc') => {
  return function (a, b) {
    if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key))
      // property doesn't exist on either object
      return 0

    const varA = typeof a[key] === 'string' ? a[key].toUpperCase() : a[key]
    const varB = typeof b[key] === 'string' ? b[key].toUpperCase() : b[key]

    let comparison = 0
    if (varA > varB) comparison = 1
    else if (varA < varB) comparison = -1

    return order == 'desc' ? comparison * -1 : comparison
  }
}

export const getAge = (age) => {
  const now = moment().diff(moment(age), 'years')
  return now
}

export const ageRang = (ageRage, age) => {
  let data = []
  ageRage.map((ages) => {
    const arr = ages.split('-')
    data = [...data, ...arr]
  })
  const min = Math.min(...data)
  const max = Math.max(...data)
  // console.log('ageRang', { data, min, max, age, return: age > min && age < max })
  return age > min && age < max
}

export const ageDuration = (sessionTime) => {
  const h = Math.floor(sessionTime / 3600)
  const m = Math.floor((sessionTime % 3600) / 60)
  const s = Math.floor((sessionTime % 3600) % 60)
  // console.log('displayTime', { h, m, s })
  if (h > 0) return `${h * 60} minute`
  else if (m > 0) return `${m} minute`
  return
}

export const groupAgeAndGetAgeRange = (ages, LEVEL, ageRang) => {
  let objecAge = {}
  ages.map((age, index) => {
    const arr = age.split('-')
    objecAge['age' + index] = age.split('-')
  })
  let output = []
  Object.keys(objecAge).map((key) => {
    const min = Math.min(...objecAge[key])
    const max = Math.max(...objecAge[key])
    if ((ageRang > min) & (ageRang < max)) {
      output = [`${objecAge[key][0]}-${objecAge[key][1]}`]
    }
    // console.log('objecAge', { objecAge: objecAge[key], key, min, max })
  })
  const findAgeRang = LEVEL.find((item) => output.includes(item.AGE))
  return findAgeRang
}

export const displayAgeRang = (ageRang) => {
  const startAge = 0
  const endAge = 99
  const arrAge = ageRang.split('-')
  if ((arrAge[0] > 0) & (arrAge[1] == 99)) {
    // console.log(`${arrAge[0]} years old and above`)
    return `${arrAge[0]} years old and above`
  } else if ((arrAge[0] > 0) & (arrAge[1] < 99)) {
    // console.log(`${arrAge[0]} - ${arrAge[1]} years`)
    return `${arrAge[0]} - ${arrAge[1]} years`
  } else {
    // console.log('all ages')
    return 'All ages'
  }
}

export const splitCourseName = (courseName) => {
  const arrCourse = courseName.split('/')
  return arrCourse
}

export const getDayLists = (month, year) => {
  const days = [
    {
      label: '01',
      value: '01',
    },
    {
      label: '02',
      value: '02',
    },
    {
      label: '03',
      value: '03',
    },
    {
      label: '04',
      value: '04',
    },
    {
      label: '05',
      value: '05',
    },
    {
      label: '06',
      value: '06',
    },
    {
      label: '07',
      value: '07',
    },
    {
      label: '08',
      value: '08',
    },
    {
      label: '09',
      value: '09',
    },
    {
      label: '10',
      value: '10',
    },
    {
      label: '11',
      value: '11',
    },
    {
      label: '12',
      value: '12',
    },
    {
      label: '13',
      value: '13',
    },
    {
      label: '14',
      value: '14',
    },
    {
      label: '15',
      value: '15',
    },
    {
      label: '16',
      value: '16',
    },
    {
      label: '17',
      value: '17',
    },
    {
      label: '18',
      value: '18',
    },
    {
      label: '19',
      value: '19',
    },
    {
      label: '20',
      value: '20',
    },
    {
      label: '21',
      value: '21',
    },
    {
      label: '22',
      value: '22',
    },
    {
      label: '23',
      value: '23',
    },
    {
      label: '24',
      value: '24',
    },
    {
      label: '25',
      value: '25',
    },
    {
      label: '26',
      value: '26',
    },
    {
      label: '27',
      value: '27',
    },
    {
      label: '28',
      value: '28',
    },
    {
      label: '29',
      value: '29',
    },
    {
      label: '30',
      value: '30',
    },
    {
      label: '31',
      value: '31',
    },
  ]
  if (parseInt(month) == 2) {
    if ((parseInt(year) % 4 === 0 && parseInt(year) % 100 != 0) || parseInt(year) % 400 == 0) {
      days.splice(29, 2)
    } else {
      days.splice(28, 3)
    }
  } else if (
    parseInt(month) == 4 ||
    parseInt(month) == 6 ||
    parseInt(month) == 9 ||
    parseInt(month) == 11
  ) {
    days.splice(30, 1)
  }
  return days
}

export const getMonthLists = () => {
  // const month = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12']
  const month = [
    {
      label: 'January',
      value: '01',
    },
    {
      label: 'February',
      value: '02',
    },
    {
      label: 'March',
      value: '03',
    },
    {
      label: 'April',
      value: '04',
    },
    {
      label: 'May',
      value: '05',
    },
    {
      label: 'June',
      value: '06',
    },
    {
      label: 'July',
      value: '07',
    },
    {
      label: 'August',
      value: '08',
    },
    {
      label: 'September',
      value: '09',
    },
    {
      label: 'October',
      value: '10',
    },
    {
      label: 'November',
      value: '11',
    },
    {
      label: 'December',
      value: '12',
    },
  ]
  return month
}

export const getYearLists = () => {
  //   const DATE_REGEXP = new RegExp('^(0?[1-9]|[1-2][0-9]|3[0-1])/(0?[1-9]|1[0-2])/([0-9]{4})$', 'gi');
  // dateStr = dateStr.replace(DATE_REGEXP,
  //     (str: string, day: string, month: string, year: string) => {
  //         return `${day}/${month}/${parseInt(year, 10) + 543}`;
  // });

  const oldYear = new Date('1930').getFullYear()
  const newYear = new Date().getFullYear()
  const year = []
  for (var i = newYear; i >= oldYear; i--) {
    year.push({
      label: `${i}/${parseInt(i, 10) + 543}`,
      value: i,
    })
  }
  return year
}
