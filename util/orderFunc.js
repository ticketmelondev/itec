export const orderDetailFomat = (orders) => {
  return {
    ...orders,
    DETAIL: {
      ...orders.DETAIL,
      ORDER_SHOT_ID: orderShotId(orders.DETAIL.ORDER_ID),
    },
  }
}

const orderShotId = (orderId) => {
  let result = ''
  let splitId = ''
  const first6Digi = orderId.substring(0, 6)
  orderId.split('-').map((i) => {
    splitId = splitId + i
  })
  for (let i = 0; i < 4; i++) {
    result += splitId.charAt(Math.floor(Math.random() * splitId.length))
  }
  return first6Digi + result
}
