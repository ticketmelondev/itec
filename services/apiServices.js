// require('es6-promise').polyfill()
import fetch from 'isomorphic-unfetch'

export const getAPIService = async (url, header) => {
  const urlCall = `${url}`
  const headers = buildHeader(header)
  const res = await fetch(urlCall, {
    method: 'GET',
    headers,
  })
    .then((response) => {
      return response.json()
    })
    .then((json) => {
      if (json.statusCode === '0' || json.statusCode === 0) return { status: true, message: json }
      else return { status: false, message: json }
    })
    .catch((ex) => {
      return { status: false, message: ex }
    })
  return res
}

export const postAPIService = async (url, header, body) => {
  const headers = buildHeader(header)
  const res = await fetch(url, {
    method: 'POST',
    headers,
    body: body,
  })
    .then((response) => {
      return response.json()
    })
    .then((json) => {
      if (json.statusCode === '0' || json.statusCode === 0) return { status: true, message: json }
      else return { status: false, message: json }
    })
    .catch((ex) => {
      console.log('catch', ex)
      return { status: false, message: ex }
    })
  return res
}

export const putAPIService = async (url, header, body) => {
  const headers = buildHeader(header)
  const res = await fetch(url, {
    method: 'PUT',
    headers,
    body: body,
  })
    .then((response) => {
      return response.json()
    })
    .then((json) => {
      if (json.statusCode === '0' || json.statusCode === 0) return { status: true, message: json }
      else return { status: false, message: json }
    })
    .catch((ex) => {
      return { status: false, message: ex }
    })
  return res
}

export const delAPIService = async (url, header, body) => {
  const headers = buildHeader(header)
  const res = await fetch(url, {
    method: 'DELETE',
    headers,
    body: body,
  })
    .then((response) => {
      return response.json()
    })
    .then((json) => {
      if (json.statusCode === '0' || json.statusCode === 0) return { status: true, message: json }
      else return { status: false, message: json }
    })
    .catch((ex) => {
      return { status: false, message: ex }
    })
  return res
}

export const uploadFile = async (url, header, files) => {
  const headers = buildHeader(header)
  const res = await fetch(url, {
    method: 'POST',
    headers,
    body: files,
  })
    .then((response) => {
      return response.json()
    })
    .then((json) => {
      if (json.statusCode === '0') return { status: true, message: json }
      else return { status: false, message: json }
    })
    .catch((ex) => {
      return { status: false, message: ex }
    })
  return res
}

export const buildHeader = (header) => {
  const result = {
    ...header,
  }
  return result
}
export const buildHeaderForUploadFiles = (header) => {
  const result = {
    ...header,
    'Content-Type': 'multipart/form-data',
  }
  return result
}

export const setHeaderContentLanguage = (language) => {
  const lang = setContentLanguage(language)
  return {
    'Content-Language': lang,
    'Accept-Language': lang,
  }
}
export const setContentLanguage = (language) => {
  let result = 'en-us'
  switch (language) {
    case 'TH':
      result = 'th'
      break
    case 'EN':
      result = 'en-us'
      break
    default:
      result = 'en-us'
  }
  return result
}
