export const dayOfClass = [
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
  'Sunday',
]

export const startDate = [
  '12 May 2020',
  '19 May 2020',
  '26 May 2020',
  '2 Jun 2020',
  '9 Jun 2020',
  '16 Jun 2020',
  '23 Jun 2020',
]

export const timePeriod = ['08:00 - 12:00', '13:00 - 17:00', '18:00 - 22:00']
