module.exports = async function () {
  return {
    '/': {
      page: '/index',
    },
    '/course': {
      page: '/course',
    },
    '/payment': {
      page: '/payment',
    },
    '/mycourse': {
      page: '/my-course',
    },
    '/about': {
      page: '/static-about',
    },
    '/blog/1': { page: '/blog/[id]' },
  }
}
