import React, { useEffect } from 'react'
import { bindActionCreators, compose } from 'redux'
import { nextConnect } from '@redux/store'
import * as Actions from '@actions/orderActions'

import withPage from '@containers/HigherOrderComponent/withPage'

import Button from '@components/Button'
import Icon from '@components/Icon'
import Link from '@components/Link'

import { makeStyles } from '@material-ui/core/styles'

const ThankYouStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh',
    backgroundColor: '#E5E5E5',
  },
  paper: {
    width: 902,
    background: '#FFFFFF',
    borderRadius: 5,
    padding: 21,
    textAlign: 'center',
    [theme.breakpoints.down('xs')]: {
      paddingLeft: 44,
      paddingRight: 44,
    },
  },
  imageRoot: {
    marginTop: 87,
    marginBottom: 40,
  },
  spanRoot: {
    marginBottom: 4,
  },
  span: {
    fontSize: 16,
    lineHeight: '24px',
    textAlign: 'center',
    letterSpacing: '0.02em',
    color: '#7A7A7A',
  },
  courseNameRoot: {
    width: 437,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: 48,
    [theme.breakpoints.down('xs')]: {
      width: '100%',
    },
  },
  courseName: {
    fontSize: 24,
    lineHeight: '32px',
    textAlign: 'center',
    textTransform: 'capitalize',
    color: 'rgba(0, 0, 0, 0.8)',
  },
  detailRoot: {
    width: 327,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: 16,
  },
  detail: {
    fontSize: 16,
    lineHeight: '24px',
    textAlign: 'center',
    letterSpacing: '0.02em',
    color: 'rgba(0, 0, 0, 0.7)',
  },
  View: {
    marginBottom: 16,
  },
  browse: {
    width: 360,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: 92,
  },
}))

import classNames from 'classnames'
import { getStatic } from '@lib/static'

function getAuthDataFromCallbackURL(query) {
  const urlParams = new URLSearchParams(query)
  const token = urlParams.get('order') || false
  return {
    order_id: token,
  }
}

function ThankYou({ orderDetails, actions }) {
  const classes = ThankYouStyles()
  useEffect(() => {
    const { order_id: orderIdFromURL } = getAuthDataFromCallbackURL(window.location.search)
    if (orderIdFromURL) {
      actions.initOrderThankYou(orderIdFromURL)
    }
    // console.log('orderIdFromURL', { orderIdFromURL })
  }, [])
  // console.log('orderDetails', orderDetails)

  return (
    <div className={classes.root}>
      {Object.keys(orderDetails).length > 0 && (
        <div className={classes.paper}>
          <div className={classes.imageRoot}>
            <img src={`${getStatic('static/images/mail.png')}`} alt="img" style={{ width: 174 }} />
          </div>
          <div className={classes.spanRoot}>
            <span className={classes.span}>You have been successfully enrolled to</span>
          </div>
          <div className={classes.courseNameRoot}>
            <span className={classes.courseName}>{orderDetails.COURSE_DETAIL.COURSE_NAME}</span>
          </div>
          <div className={classes.detailRoot}>
            <span className={classes.detail}>
              To Start booking your course timetable, please click on the button below
            </span>
          </div>
          <div className={classes.View}>
            <Link route="my-course" passHref>
              <Button onClick={() => {}} color="primary" size="large">
                View My Course
              </Button>
            </Link>
          </div>
          <div className={classes.browse}>
            <Link route="home" passHref>
              <Button onClick={() => {}} size="small" fullWidth>
                Browse More
                <Icon style={{ fontSize: '14px', overflow: 'unset', marginLeft: 12, height: 11 }}>
                  arrowForward
                </Icon>
              </Button>
            </Link>
          </div>
        </div>
      )}
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    orderDetails: state.order.orderDetails,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
  }
}

const enhancer = compose(
  nextConnect(mapStateToProps, mapDispatchToProps),
  withPage({ restricted: true }),
)

export default enhancer(ThankYou)
