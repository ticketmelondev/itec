import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'

import withPage from '@containers/HigherOrderComponent/withPage'

const useStyles = makeStyles((theme) => ({
  root: {
    // width: 375,
    fontSize: '14px',
    lineHeight: '160%',
    letterSpacing: '0.02em',
    textTransform: 'capitalize',
    color: 'rgba(0, 0, 0, 0.6)',
    paddingTop: 24,
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
}))

function Term() {
  const classes = useStyles()

  return (
    <div className={classes.root}>
      <Container>
        <p style={{ fontSize: 24 }}>Terms & Conditions</p>

        <p style={{ fontSize: 16 }}>Terms and Conditions</p>
        <p>
          Please carefully read the following Terms and Conditions before using the website
          (&quot;Site&quot;). These Terms and Conditions apply to all visits and use of the Site,
          and to the Contents (see the definition below), information, recommendations and/or
          services of the Site or provided through the Site. Once you visit and use the Site, you
          fully agree to comply with all the Terms and Conditions as well as other laws or
          regulations applied to the Site and internet. If you do not agree to accept these Terms
          and Conditions, please leave the Site immediately. The Site owns the relevant intellectual
          property rights and derivative rights to the information. Any unauthorized use of the
          information on the Site may violate the copyright law, the trademark law and other laws.
        </p>

        <p style={{ fontSize: 16 }}>Content Ownership</p>
        <p>
          The Site is owned and operated by Eduworld. All the contents displayed by or shown on the
          Site, including but not limited to text, graphics, photographs, images, animations, sound,
          illustrations and software (&quot;Content&quot;) are owned by Eduworld and/or its group
          companies (hereinafter referred to &quot;Eduworld&quot;), licensors, and/or content
          suppliers. All the elements of the Site, including but not limited to overall design and
          the Content, are subject to legal protection of trade dress, copyright, ethics, trademarks
          and all other relevant intellectual property rights. Unless based on these Terms and
          Conditions or other agreements via Eduworld statement, any part, elements or Content of
          the Site shall not be reproduced or transmitted in any way. Unless with explicit
          agreements, the Content and all related rights are assets of Eduworld group companies or
          its licensors.
        </p>

        <p style={{ fontSize: 16 }}>Disclaimer</p>
        <p>
          Within the extent permitted by law, the Site is provided &quot;as is&quot; without any
          types of warranties, express or implied, including but not limited to implied guarantee of
          satisfactory quality, fitness for a particular purpose and non-infringement. The
          information on the Site is provided only for general purpose. Nothing on the Site
          constitutes advice of any kind. Eduworld reserves the right to suspend or revoke the
          entire Site or any part of it, without notice or liability.Eduworld is entitled to change
          the information and services on the Site at any time without notice. The information and
          services on the Site may have expired. Eduworld is uncommitted on updates of the
          information and services on the Site. Eduworld doesn&#39;t guarantee or confirm that the
          information and / or programs contained within the Site is accurate, complete or current;
          or the Site will not be interrupted or error will not occurr; or the existence of any
          defects on the Site must be corrected; or the Site or the server providing the services do
          not contain viruses or any other harmful components. Within the extent permitted by law,
          when you use the materials on the Site, Eduworld doesn&#39;t guarantee or make
          confirmation of the accuracy, appropriateness, usefulness, timeliness, reliability or
          other aspects of the materials.
        </p>

        <p style={{ fontSize: 16 }}>Limitation of Liability</p>
        <p>
          You are liable for any risks when using the Site. Eduworld and its affiliates, other
          third-party vendors, officers, directors, agents or anyone involved in the design,
          creation and delivery of the content of the Site are not liable for any direct or indirect
          lose results (regardless of system based on warranty, contract, tort or any other legal
          opinions, and whether there is notice in advance the possibility of such damages) caused
          by using or not using the information, data or services contained in the Site, other
          websites linked from the Site or any/all sites of the kind (including, but not limited to,
          lost profits, lost data or business interruption caused by the damage). Eduworld&#39;s
          consistent policy is to insist on not dealing with or accepting any unsolicited
          recommendations, inventions, designs, confidential or patentable information and/or other
          materials from any person, whether they are made up of text, images, audio, software,
          information or otherwise (the &quot;Materials&quot;). Therefore, you should not post any
          Material on the Site, or send the Materials to Eduworld by e-mail or other means. Eduworld
          will not assume any responsibility for these Materials. Eduworld reserves the sole
          discretion to block or remove anything not complying with the Terms and Conditions, or any
          communication Content and Materials which are not welcomed by Eduworld. The Site is
          prohibited from posting or transmitting any unlawful, threatening, libelous, defamatory,
          obscene, pornographic or other material that may violate any laws. Sometimes Eduworld
          allows you to publicly issue content created by yourself to the Site members or others
          through ways such as electronic posters, BBS, postcards, etc. If you use these services,
          that means you acknowledge that Eduworld serves only as a passive channel and has no
          responsibility and obligations for the communication contents and materials created by you
          or other users.
        </p>

        <p style={{ fontSize: 16 }}>Amendments to Terms and Conditions</p>
        <p>
          Eduworld reserves the discretion to change, modify, add or remove the contents of the
          Terms and Conditions. Please check the Terms and Conditions regularly to learn about
          relevant changes. After the amendments to the Terms and Conditions (including, but not
          limited to, the privacy policies of Eduworld) are announced, if you continue to use the
          Site, that will indicate your acceptance of such changes.
        </p>
      </Container>
    </div>
  )
}

export default withPage()(Term)
