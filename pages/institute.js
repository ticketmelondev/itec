import React, { Fragment, useEffect } from 'react'
import { bindActionCreators, compose } from 'redux'
import { nextConnect } from '@redux/store'
import * as Actions from '@actions/courseActions'
import { getStatic } from '@lib/static'

import withPage from '@containers/HigherOrderComponent/withPage'
import Courses from '@containers/Courses'

function getAuthDataFromCallbackURL(query) {
  const urlParams = query.split('/')[1]
  const slug = urlParams || false
  return {
    slug,
  }
}

function Institute({ actions, instituteDetail, bannerLoading, courseLoading }) {
  useEffect(() => {
    const { slug: slugFromURL } = getAuthDataFromCallbackURL(window.location.pathname)
    if (slugFromURL) {
      actions.initCourses(slugFromURL)
      actions.initInstituteDetail(slugFromURL)
    }
    console.log('slugFromURL', { slugFromURL })
  }, [])
  // console.log('instituteDetail', instituteDetail)
  return (
    <Fragment>
      <div style={{ width: '100%' }}>
        {bannerLoading ? null : (
          <img
            src={instituteDetail.IMG_BANNER}
            // alt="banner"
            style={{ width: '100%', height: '100%' }}
          />
        )}
      </div>
      {courseLoading ? null : <Courses instituteDetail={instituteDetail} />}
    </Fragment>
  )
}

const mapStateToProps = (state) => {
  return {
    instituteDetail: state.courses.instituteDetail,
    bannerLoading: state.isLoading.instituteBannerLoading,
    courseLoading: state.isLoading.courseLoading,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
  }
}

const enhancer = compose(nextConnect(mapStateToProps, mapDispatchToProps), withPage())

export default enhancer(Institute)
