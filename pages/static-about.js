import React from 'react'
import withPage from '@containers/HigherOrderComponent/withPage'
import Button from '@components/Button'
import Icon from '@components/Icon'
import Select from '@components/Select'
import fetch from 'isomorphic-unfetch'
import { Grid, Typography } from '@material-ui/core'

function StaticAbout({ meow }) {
  return (
    <div className="container">
      <div>
        <Button color="primary" style={{ margin: '8px' }}>
          primary
        </Button>
        <Button color="primary" disabled style={{ margin: '8px' }}>
          primary disabled
        </Button>
        <Button color="primary" outlined style={{ margin: '8px' }}>
          primary outlined
        </Button>
        <Button color="primary" outlined disabled style={{ margin: '8px' }}>
          primary outlined disabled
        </Button>
        <Button color="primary" style={{ margin: '8px' }}>
          primary icon
          <Icon style={{ marginLeft: 15 }}>student</Icon>
        </Button>
      </div>
      <div>
        <Button style={{ margin: '8px' }}>default</Button>
        <Button disabled style={{ margin: '8px' }}>
          default
        </Button>
        <Button outlined style={{ margin: '8px' }}>
          default outline
        </Button>
        <Button outlined disabled style={{ margin: '8px' }}>
          default outline disabled
        </Button>
      </div>
      <div>
        <Button color="primary" size="small" style={{ margin: '8px' }}>
          small
        </Button>
        <Button color="primary" size="medium" style={{ margin: '8px' }}>
          medium
        </Button>
        <Button color="primary" size="large" style={{ margin: '8px' }}>
          large
        </Button>
      </div>
      <hr />
      {/** ----- Icons Line1 ----- */}
      <Grid container alignItems="center" style={{ padding: '16px' }}>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>menu</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            menu
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>server</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            server
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>briefcase</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            briefcase
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>student</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            student
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>bell</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            bell
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>activity</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            activity
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>doc</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            doc
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>dollarSign</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            dollarSign
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>messageSquare</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            messageSquare
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>calendar</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            calendar
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>clock</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            clock
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>alert</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            alert
          </Typography>
        </Grid>
      </Grid>
      {/** ----- Icons Line2 ----- */}
      <Grid container alignItems="center" style={{ padding: '16px' }}>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>editBoxOutline</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            editBoxOutline
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>edit</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            edit
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>editOutline</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            editOutline
          </Typography>
        </Grid>

        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>export</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            export
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>logout</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            logout
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>arrowUpward</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            arrowUpward
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>arrowDownward</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            arrowDownward
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>arrowBack</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            arrowBack
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>arrowForward</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            arrowForward
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>arrowUpwardCircle</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            arrowUpwardCircle
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>arrowDownwardCircle</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            arrowDownwardCircle
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>arrowBackCircle</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            arrowBackCircle
          </Typography>
        </Grid>
      </Grid>
      {/** ----- Icons Line3 ----- */}
      <Grid container alignItems="center" style={{ padding: '16px' }}>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>arrowForwardCircle</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            arrowForwardCircle
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>expandLess</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            expandLess
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>expandMore</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            expandMore
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>chevronLeft</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            chevronLeft
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>chevronRight</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            chevronRight
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>arrowDropup</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            arrowDropup
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>arrowDropdown</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            arrowDropdown
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>arrowLeft</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            arrowLeft
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>arrowRight</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            arrowRight
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>prev</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            prev
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>next</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            next
          </Typography>
        </Grid>

        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>uncheck</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            uncheck
          </Typography>
        </Grid>
      </Grid>
      <Grid container alignItems="center" style={{ padding: '16px' }}>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>checked</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            checked
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>plus</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            plus
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>plusSquare</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            plusSquare
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>facebook</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            facebook
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>twitter</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            twitter
          </Typography>
        </Grid>
        <Grid item xs={1} style={{ textAlign: 'center' }}>
          <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>youtube</Icon>
          <Typography variant="body1" style={{ fontSize: '0.75rem', color: '#9B9B9B' }}>
            youtube
          </Typography>
        </Grid>
      </Grid>
    </div>
  )
}

StaticAbout.getInitialProps = async function () {
  const res = await fetch('https://jsonplaceholder.typicode.com/users')
  const data = await res.json()

  return {
    meow: data[0].name || 'test',
  }
}

export default withPage()(StaticAbout)
