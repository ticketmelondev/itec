import React, { Fragment, useEffect } from 'react'
import dynamic from 'next/dynamic'
import { bindActionCreators, compose } from 'redux'
import { nextConnect } from '@redux/store'
import * as Actions from '@actions/orderActions'
import { useRouter } from 'next/router'
import { get } from 'lodash'

import withPage from '@containers/HigherOrderComponent/withPage'
// import Bank from '@containers/Bank'

const Bank = dynamic(() => import('@containers/Bank'), {
  ssr: false,
  loading: () => null,
})

function getAuthDataFromCallbackURL(query) {
  const urlParams = new URLSearchParams(query)
  const order_id = urlParams.get('order') || false
  return {
    order_id,
  }
}

function BankTransfer({ orderDetails, isLoading, actions }) {
  useEffect(() => {
    const { order_id: orderIdFromURL } = getAuthDataFromCallbackURL(window.location.search)
    if (orderIdFromURL) {
      actions.initOrderBankDetail(orderIdFromURL)
    }
    // console.log('orderIdFromURL', { orderIdFromURL })
  }, [])

  return (
    <Fragment>
      <Bank orderDetails={orderDetails} isLoading={isLoading} />
    </Fragment>
  )
}

const mapStateToProps = (state) => {
  return {
    orderDetails: state.order.orderDetails,
    isLoading: state.isLoading.getOrderLoading,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
  }
}

const enhancer = compose(
  nextConnect(mapStateToProps, mapDispatchToProps),
  withPage({ restricted: true }),
)

export default enhancer(BankTransfer)
