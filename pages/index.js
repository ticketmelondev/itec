import React, { Fragment, useEffect } from 'react'
import { bindActionCreators, compose } from 'redux'
import { nextConnect } from '@redux/store'
import * as Actions from '@actions/courseActions'
import { getStatic } from '@lib/static'
import { getDayLists, getMonthLists, getYearLists } from '@util/dataFormate'

import withPage from '@containers/HigherOrderComponent/withPage'
import Courses from '@containers/Courses'
import HomeComponent from '@containers/Home'

function Home({ actions }) {
  useEffect(() => {
    actions.initInstitute()
  }, [])

  return (
    <Fragment>
      {/* <div style={{ width: '100%' }}>
        <img
          src={`https://itec-public-file.s3-ap-southeast-1.amazonaws.com/banner/BANNER-WEB-ECC.jpg`}
          alt="banner"
          style={{ width: '100%', height: '100%' }}
        />
      </div>
      <Courses /> */}
      <HomeComponent />
    </Fragment>
  )
}

const mapStateToProps = (state) => {
  return {}
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
  }
}

const enhancer = compose(nextConnect(mapStateToProps, mapDispatchToProps), withPage())

export default enhancer(Home)
