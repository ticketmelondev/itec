const initState = {
  orderDetails: {},
  hashValue: '',
}
const order = (state = initState, action) => {
  switch (action.type) {
    case 'SET_ORDER_DETAILS':
      return {
        ...state,
        orderDetails: action.orderDetails,
      }
    case 'SET_HASH_VALUE':
      return {
        ...state,
        hashValue: action.hashValue,
      }
    default:
      return state
  }
}

export default order
