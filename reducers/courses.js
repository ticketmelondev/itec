const initState = {
  allCourses: [],
  course: {},
  isNotFoundCourses: false,
  institute: [],
  instituteDetail: {},
}
const courses = (state = initState, action) => {
  switch (action.type) {
    case 'SET_ALL_COURSES':
      return {
        ...state,
        allCourses: action.allCourses,
      }
    case 'SET_COURSE':
      return {
        ...state,
        course: action.course,
      }
    case 'SET_NOT_FOUND_COURS':
      return {
        ...state,
        isNotFoundCourses: action.isNotFoundCourses,
      }
    case 'SET_INSTITUTE':
      return {
        ...state,
        institute: action.institute,
      }
    case 'SET_INSTITUTE_DETAIL':
      return {
        ...state,
        instituteDetail: action.instituteDetail,
      }
    default:
      return state
  }
}

export default courses
