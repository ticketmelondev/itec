const initState = {
  page: 1,
  registerDetails: {},
  mobileStep: 'login',
}

const register = (state = initState, action) => {
  switch (action.type) {
    case 'SET_PAGE':
      return {
        ...state,
        page: action.page,
      }
    case 'SET_REGISTER_DETAILS':
      return {
        ...state,
        registerDetails: {
          ...state.registerDetails,
          ...action.details,
        },
      }
    case 'SET_MOBILE_STEP':
      return {
        ...state,
        mobileStep: action.mobileStep,
      }
    case 'CLEAR_REGISTER':
      return {
        page: 1,
        registerDetails: {},
      }
    default:
      return state
  }
}

export default register
