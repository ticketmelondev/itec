const initState = {
  open: false,
  isViewClassOpen: false,
  openBooking: false,
}

const modal = (state = initState, action) => {
  switch (action.type) {
    case 'SET_OPEN_MODAL':
      return {
        ...state,
        open: action.open,
      }
    case 'SET_VIEW_CLASS_OPEN_MODAL':
      return {
        ...state,
        isViewClassOpen: action.open,
      }
    case 'SET_BOOKING_OPEN_MODAL':
      return {
        ...state,
        openBooking: action.open,
      }
    default:
      return state
  }
}

export default modal
