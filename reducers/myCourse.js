const initState = {
  calendars: {},
  selectCalendar: [],
  myCourses: [],
  coursesWaitPayment: [],
  sessionDetail: {},
  viewSession: {},
  accountsTeacher: [],
  accountsStudent: [],
}
const myCourse = (state = initState, action) => {
  switch (action.type) {
    case 'SET_CALENDAR':
      return {
        ...state,
        calendars: action.calendar,
      }
    case 'SET_SELECT_CALENDAR':
      return {
        ...state,
        selectCalendar: action.selectCalendar,
      }
    case 'SET_MY_COURSE':
      return {
        ...state,
        myCourses: action.myCourses,
      }
    case 'SET_MY_COURSE_WAIT_PAYMENT':
      return {
        ...state,
        coursesWaitPayment: action.coursesWaitPayment,
      }
    case 'SET_SESSION_DETAIL':
      return {
        ...state,
        sessionDetail: action.sessionDetail,
      }
    case 'SET_VIEW_SESSION':
      return {
        ...state,
        viewSession: action.viewSession,
      }
    case 'SET_ACCOUNT_TEACHER_COURSE':
      return {
        ...state,
        accountsTeacher: action.accountsTeacher,
      }
    case 'SET_ACCOUNT_STUDENT_COURSE':
      return {
        ...state,
        accountsStudent: action.accountsStudent,
      }
    default:
      return state
  }
}

export default myCourse
