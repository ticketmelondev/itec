const initState = {
  bookingDetail: {},
}
const booking = (state = initState, action) => {
  switch (action.type) {
    case 'SET_BOOKING_DETAIL':
      return {
        ...state,
        bookingDetail: action.bookingDetail,
      }
    default:
      return state
  }
}

export default booking
