export const setOpenModal = (open) => {
  return {
    type: 'SET_OPEN_MODAL',
    open,
  }
}

export const setViewClassOpenModal = (open) => {
  return {
    type: 'SET_VIEW_CLASS_OPEN_MODAL',
    open,
  }
}

export const setMobileStep = (mobileStep) => {
  return {
    type: 'SET_MOBILE_STEP',
    mobileStep,
  }
}
