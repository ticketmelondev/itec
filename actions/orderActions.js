import { postAPIService, getAPIService, putAPIService } from '@services/apiServices'
import { getAPIRouter } from '@config/APIConfig'
import { VERSION_2C2P, MERCHANT_ID, CURRENCY } from '@config/constants'
import moment from 'moment'
import { toNumber, calculatAmount12Digit } from '@util/dataFormate'
import { orderDetailFomat } from '@util/orderFunc'

export const setOrderDetails = (orderDetails) => {
  return {
    type: 'SET_ORDER_DETAILS',
    orderDetails,
  }
}
export const setHashValue = (hashValue) => {
  return {
    type: 'SET_HASH_VALUE',
    hashValue,
  }
}
export const setOrderLoading = (isLoading) => {
  return {
    type: 'SET_GET_ORDER_LOADING',
    isLoading,
  }
}

export const initOrder = (order_id) => async (dispatch, getState) => {
  const account = getState().account

  const url = getAPIRouter('GET_ORDER')
  const finalUrl = url.replace('param', order_id)
  const header = {
    'Content-Type': 'application/json',
    Authorization: account.token,
  }
  dispatch(setOrderLoading(true))

  const res = await getAPIService(finalUrl, header)
  if (res.status) {
    const items = orderDetailFomat(res.message.data)
    console.log(items)
    dispatch(setOrderDetails(items))
    dispatch(getHash(items))
  } else {
    console.error('error initOrder', res)
  }
  dispatch(setOrderLoading(false))
}

export const initOrderBankDetail = (order_id) => async (dispatch, getState) => {
  const account = getState().account

  const url = getAPIRouter('GET_ORDER')
  const finalUrl = url.replace('param', order_id)
  const header = {
    'Content-Type': 'application/json',
    Authorization: account.token,
  }
  dispatch(setOrderLoading(true))

  const res = await getAPIService(finalUrl, header)
  if (res.status) {
    const items = res.message.data
    console.log(items)
    dispatch(setOrderDetails(items))
  } else {
    console.error('error', res)
  }
  dispatch(setOrderLoading(false))
}

export const getHash = (data) => async (dispatch, getState) => {
  const account = getState().account
  const timeStam = moment(data.DETAIL.CREATED_AT).valueOf()

  const merchant_id = MERCHANT_ID
  const currency = CURRENCY
  const result_url_1 = getAPIRouter('RESULT_PAYMENT')
  const version = VERSION_2C2P
  const user_defined_1 = data.DETAIL.ORDER_ID
  // const order_id = timeStam.toString().substring(0, 12)
  const order_id = data.DETAIL.ORDER_SHOT_ID

  const amount = calculatAmount12Digit(data.DETAIL.PRICE)

  const payment_description = data.COURSE_DETAIL.COURSE_NAME

  const url = getAPIRouter('GET_HASH')
  const header = {
    'Content-Type': 'application/json',
    Authorization: account.token,
  }
  const params =
    version +
    merchant_id +
    payment_description +
    order_id +
    currency +
    amount +
    user_defined_1 +
    result_url_1

  const body = {
    ORDER_ID: data.DETAIL.ORDER_ID,
    PAYLOAD: params,
  }

  const res = await postAPIService(url, header, JSON.stringify(body))
  if (res.status) {
    console.log('getHash', res)
    dispatch(setHashValue(res.message.data))
  } else {
    console.error('error getHash', res)
  }
}

export const updateOrder = (data) => async (dispatch, getState) => {
  const account = getState().account
  const {
    orderDetails: { DETAIL },
  } = getState().order
  console.log('updateOrder', DETAIL)

  const url = getAPIRouter('GET_ORDER')
  const finalUrl = url.replace('param', DETAIL.ORDER_ID)
  const header = {
    'Content-Type': 'application/json',
    Authorization: account.token,
  }
  const body = {
    PAYMENT_TYPE: data,
  }

  const res = await putAPIService(finalUrl, header, JSON.stringify(body))
  if (res.status) {
    console.log('updateOrder', res)
  } else {
    console.error('error updateOrder', res)
  }
}

export const initOrderThankYou = (order_id) => async (dispatch, getState) => {
  const account = getState().account

  const url = getAPIRouter('GET_ORDER')
  const finalUrl = url.replace('param', order_id)
  const header = {
    'Content-Type': 'application/json',
    Authorization: account.token,
  }
  dispatch(setOrderLoading(true))

  const res = await getAPIService(finalUrl, header)
  if (res.status) {
    const items = orderDetailFomat(res.message.data)
    dispatch(setOrderDetails(items))
  } else {
    console.error('initOrderThankYou error', res)
  }
  dispatch(setOrderLoading(false))
}
