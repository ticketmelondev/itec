import { postAPIService } from '@services/apiServices'
import { getAPIRouter } from '@config/APIConfig'
import { toTimeSteamp } from '@util/bookingFunc'
import { groupAgeAndGetAgeRange } from '@util/dataFormate'
import { setAlert } from './alertActions'
import { initCourseStudentOrTeacher } from './courseActions'
import { bookandrescheduleflow } from '@util/myCourseFunc'
import moment from 'moment'

export const setBookingDetail = (bookingDetail) => {
  return {
    type: 'SET_BOOKING_DETAIL',
    bookingDetail,
  }
}

export const setOrderLoading = (isLoading) => {
  return {
    type: 'SET_ORDER_LOADING',
    isLoading,
  }
}

export const setOpenBookingModal = (open) => {
  return {
    type: 'SET_BOOKING_OPEN_MODAL',
    open,
  }
}

export const setCreateSessionLoading = (isLoading) => {
  return {
    type: 'SET_CREATE_SESSION_LOADING',
    isLoading,
  }
}

// export const initBookingDetail = (index) => async (dispatch, getState) => {
//   const { course } = getState().courses
//   await dispatch(setBookingDetail(course.LEVEL[index]))
//   dispatch(createOrder(moment().format('D MMM YYYY')))
// }

export const initBookingDetail = (ages, ageRang) => async (dispatch, getState) => {
  dispatch(createOrder(moment().format('D MMM YYYY'), ages, ageRang))
}

export const initTecherSchdule = ({ value }) => async (dispatch, getState) => {
  const account = getState().account
  const bookingDetail = getState().booking.bookingDetail
  // console.log({
  //   account,
  //   bookingDetail,
  //   value,
  //   moment: moment.utc(value, 'D MMM YYYY').valueOf(),
  // })

  const url = getAPIRouter('SERVICE_CHECK_TECHER_SCHDULE')
  const header = {
    'Content-Type': 'application/json',
    Authorization: account.token,
  }
  const body = {
    COURSE_ID: bookingDetail.COURSE_ID,
    TYPE: bookingDetail.TYPE,
    DATE: moment.utc(value, 'D MMM YYYY').valueOf(),
  }

  const res = await postAPIService(url, header, JSON.stringify(body))
  if (res.status) {
    console.log(res)
    return res
  } else {
    console.error('error', res)
    return res
  }
}

export const createOrder = (data, ages, ageRang) => async (dispatch, getState) => {
  const account = getState().account
  const {
    course: { DETAIL, LEVEL },
  } = getState().courses

  const url = getAPIRouter('CREATE_ORDER')
  const header = {
    'Content-Type': 'application/json',
    Authorization: account.token,
  }
  const findAgeRang = groupAgeAndGetAgeRange(ages, LEVEL, ageRang)

  // console.log('object', { DETAIL, LEVEL, ages, findAgeRang, ageRang })
  const body = {
    COURSE_ID: DETAIL.COURSE_ID,
    // TYPE: bookingDetail.TYPE,
    DATE: moment(data, 'D MMM YYYY').valueOf(),
    PAYMENT_TYPE: 'bank_tran',
    SESSION_QUANTITY: findAgeRang.SESSION_QUANTITY,
    SESSION_TIME: findAgeRang.SESSION_TIME,
  }
  dispatch(setOrderLoading(true))

  const res = await postAPIService(url, header, JSON.stringify(body))
  if (res.status) {
    console.log(res)
    const items = res.message.data
    window.location = `/payment?order=${items.ORDER_ID}`
  } else {
    console.error('error', res)
    dispatch(
      setAlert({
        text: res.message.data,
        type: 'error',
      }),
    )
  }
  dispatch(setOrderLoading(false))
}

export const createSession = (fromState) => async (dispatch, getState) => {
  const account = getState().account
  const { sessionDetail } = getState().myCourse

  const url = getAPIRouter('CREATE_SESSION')
  const finalUrl = url.replace('param', sessionDetail.ORDER_ID)
  const header = {
    'Content-Type': 'application/json',
    Authorization: account.token,
  }
  const class_time = `${fromState.time_hour.value}:${fromState.time_min.value}`
  const SESSION_START_TIME = toTimeSteamp(
    fromState.type.value,
    fromState.date,
    class_time,
    fromState.session.value,
  )
  const body = {
    SESSION_START_TIME,
  }
  // console.log('createSession', {
  //   fromState,
  //   SESSION_START_TIME,
  //   sessionDetail,
  //   bookandrescheduleflow: bookandrescheduleflow(
  //     SESSION_START_TIME[0],
  //     sessionDetail.CREATED_AT,
  //     sessionDetail.COURSE_DETAIL.DETAIL.CREATE_TICKET_TIME,
  //     sessionDetail.COURSE_DETAIL.DETAIL.CREATE_TICKET_TIME_LIMIT,
  //   ),
  // })
  dispatch(setCreateSessionLoading(true))
  if (
    bookandrescheduleflow(
      SESSION_START_TIME[0],
      sessionDetail.CREATED_AT,
      sessionDetail.COURSE_DETAIL.DETAIL.CREATE_TICKET_TIME,
      sessionDetail.COURSE_DETAIL.DETAIL.CREATE_TICKET_TIME_LIMIT,
    )
  ) {
    const res = await postAPIService(finalUrl, header, JSON.stringify(body))
    if (res.status) {
      console.log(res)
      const items = res.message.data
      await dispatch(initCourseStudentOrTeacher())
      dispatch(setOpenBookingModal(false))
      dispatch(
        setAlert({
          text: 'Booking success!',
          type: 'success',
        }),
      )
      dispatch(setCreateSessionLoading(false))
      return res
    } else {
      console.error('error', res)
      dispatch(
        setAlert({
          text: res.message.data,
          type: 'error',
        }),
      )
      dispatch(setCreateSessionLoading(false))
      return res
    }
  } else {
    dispatch(
      setAlert({
        text: SESSION_START_TIME,
        type: 'error',
      }),
    )
  }
  dispatch(setCreateSessionLoading(false))
}
