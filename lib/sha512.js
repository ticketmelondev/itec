import sha512 from 'sha512'

export default (values) => sha512(values).toString('hex')
