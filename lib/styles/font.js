import { getStatic } from '../static'

export const config = {
  // google: {
  //   families: ['Open Sans:400'],
  // },
  custom: {
    urls: [
      getStatic('static/css/fonts.css'),
      // 'https://fonts.googleapis.com/css2?family=Barlow:wght@300;500;600&display=swap',
    ],
  },
}
