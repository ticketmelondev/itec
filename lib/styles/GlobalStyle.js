import React, { Fragment } from 'react'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
  '@global': {
    '*': {
      boxSizing: 'border-box',
    },
    '*:before, *:after': {
      boxSizing: 'border-box',
    },

    'html, body': {
      height: '100%',
      position: 'relative',
      margin: 0,
      letterSpacing: '0.2px',
      fontFamily: `'Inter', sans-serif`,
      fontSize: 14,
    },
    '.main-container': {
      minHeight: '100vh',
      overflow: 'hidden',
      display: 'block',
      position: 'relative',
      paddingBottom: '215px',
    },
    '.main': {
      marginTop: 72,
    },
    footer: {
      position: 'absolute',
      bottom: 0,
      width: '100%',
    },
  },
})

export default function GlobalCss() {
  useStyles()

  return <Fragment />
}
