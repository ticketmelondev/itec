import React, { Fragment, useState, useEffect } from 'react'
import dynamic from 'next/dynamic'
import { bindActionCreators } from 'redux'
import { nextConnect } from '@redux/store'
import * as Actions from '@actions/modalActions'

import FlowAccessAccountStyles from './FlowAccessAccountStyles'

const Modal = dynamic(() => import('@components/Modal'), {
  ssr: false,
  loading: () => null,
})
const Login = dynamic(() => import('@containers/Login'), {
  ssr: false,
  loading: () => null,
})
const Register = dynamic(() => import('@containers/Register'), {
  ssr: false,
  loading: () => null,
})
const ForgotPassword = dynamic(() => import('@containers/ForgotPassword'), {
  ssr: false,
  loading: () => null,
})

function FlowAccessAccount({ modal, isLoading, actions, mobileStep }) {
  const [step, setStep] = useState('login')
  const { open } = modal
  const classes = FlowAccessAccountStyles()
  const handleClose = () => {
    actions.setOpenModal(!open)
    actions.setMobileStep('login')
  }

  useEffect(() => {
    setStep(mobileStep)
  }, [open])

  return (
    <Fragment>
      {step === 'forgot' ? (
        <ForgotPassword open={open} onClose={handleClose} />
      ) : (
        <Modal open={open} onClose={handleClose} bgColor="#E5E5E5">
          <div className={classes.root}>
            {step === 'login' && <Login setStep={setStep} />}
            {step === 'register' && <Register setStep={setStep} />}
          </div>
        </Modal>
      )}
    </Fragment>
  )
}

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
    isLoading: state.isLoading.accessAccount,
    mobileStep: state.register.mobileStep,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
  }
}

export default nextConnect(mapStateToProps, mapDispatchToProps)(FlowAccessAccount)
