import React from 'react'
import moment from 'moment'
import { toNumber, calculatAmount12Digit } from '@util/dataFormate'
import classNames from 'classnames'

import Grid from '@material-ui/core/Grid'

import IconRounded from '@components/IconRounded'
import Icon from '@components/Icon'

import BankStyles from './BankStyles'

import { getStatic } from '@lib/static'

function BankDetail() {
  const classes = BankStyles()

  return (
    <div className={classNames(classes.paper, classes.BankDetailRoot)}>
      <div style={{ marginBottom: 16 }}>
        <IconRounded
          title="Important"
          icon={<Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>messageSquare</Icon>}
        />
      </div>
      <div className={classes.BankDetailTitle}>
        <span className="span1">To confirm your payment, </span>
        <span>please contact us with the following information:</span>
      </div>
      <div className={classes.BankDetailList}>
        <ul>
          <li>Order No.</li>
          <li>Course Name</li>
          <li>Image of your Payment Slip</li>
        </ul>
      </div>
      <div className={classes.BankDetailContact}>
        <span>Contact Channel:</span>
        <Grid container>
          <Grid item xs={3}>
            Line
          </Grid>
          <Grid item xs={9} style={{ fontWeight: 600 }}>
            <a style={{ color: '#4D4D4D' }} href="http://line.me/ti/p/~@eduworld">
              @eduworld
            </a>
          </Grid>
        </Grid>
        <Grid container>
          <Grid item xs={3}>
            Email
          </Grid>
          <Grid item xs={9} style={{ fontWeight: 600 }}>
            info@eduworld.ac.th
          </Grid>
        </Grid>
      </div>
    </div>
  )
}

function BankSlip({ details }) {
  const { DETAIL, COURSE_DETAIL } = details
  const classes = BankStyles()
  const tomorrow = moment(DETAIL.CREATED_AT).add(DETAIL.ORDER_EXPIRATION_TIME, 's').valueOf()

  return (
    <div className={classNames(classes.paper, classes.BankSlipRoot)}>
      <div className={classes.BankSlipHeader}>
        <p>Please transfer to the following account. Payment must be made within</p>
        <p style={{ fontWeight: 'bold' }}>{`${moment(tomorrow).format('MMM D, YYYY • HH:mm')}`}</p>
      </div>
      <div className={classes.BankSlipAmount}>
        <span className="amount">Amount:</span>
        <span className="price">{`${toNumber(DETAIL.PRICE, '0,0')} THB`}</span>
      </div>
      <div className={classes.BankSlipQrcode}>
        <img
          className={classes.BankSlipQrcode}
          src="https://itec-public-file.s3-ap-southeast-1.amazonaws.com/qrcode.png"
          alt="img"
        />
      </div>
      <div className={classes.BankSlipText}>EDUWORLD</div>
      <div className={classes.BankSlipNumber}>152 - 470179 - 2</div>
      <div className={classes.BankSlipImage}>
        <img src={`${getStatic('static/images/prompt.png')}`} alt="img" style={{ width: 73 }} />
      </div>
    </div>
  )
}

function Bank({ orderDetails, isLoading }) {
  const classes = BankStyles()
  // console.log('Bank', { orderDetails })

  return (
    <div className={classes.root}>
      {isLoading ? (
        <div>Loading...</div>
      ) : (
        Object.keys(orderDetails).length > 0 && (
          <div>
            <BankSlip details={orderDetails} />
            <BankDetail />
          </div>
        )
      )}
    </div>
  )
}

export default Bank
