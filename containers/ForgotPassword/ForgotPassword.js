import React, { useState } from 'react'
import { bindActionCreators } from 'redux'
import { nextConnect } from '@redux/store'
import * as Actions from '@actions/registerActions'
import * as AlertActions from '@actions/alertActions'
import { useForm } from 'react-hook-form'
import jwtDecode from 'jwt-decode'
import moment from 'moment'
import { schemaRequest, schemaNewpassword, schemaValidate } from './validate'

import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import Avatar from '@material-ui/core/Avatar'
import CheckIcon from '@material-ui/icons/Check'
import Collapse from '@material-ui/core/Collapse'

import Modal from '@components/Modal'
import IconRounded from '@components/IconRounded'
import Input from '@components/Input'
import Button from '@components/Button'
import Icon from '@components/Icon'
import { Alert } from '@components/Snackbar'

import ForgotPasswordStyles, { SubmitSuccessStyles } from './ForgotPasswordStyles'

function Request({ actions }) {
  const {
    register,
    handleSubmit,
    errors,
    formState: { dirty, isSubmitting },
  } = useForm({ validationResolver: schemaValidate, validationContext: schemaRequest })
  const onSubmit = (data) => {
    actions(data)
  }
  const classes = ForgotPasswordStyles()

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className={classes.titleRoot}>
        <span className={classes.title}>
          Please enter the email address associated with your account
        </span>
      </div>
      <div className={classes.inputRoot}>
        <Input
          id="email-input"
          name="EMAIL"
          label="Email"
          className={classes.margin}
          inputRef={register}
          error={errors.EMAIL}
          errorText={errors.EMAIL && errors.EMAIL.message}
          fullWidth
          outlined
        />
      </div>
      <div className={classes.buttonRoot}>
        <Button type="submit" size="large" fullWidth disabled={!dirty}>
          Submit
        </Button>
      </div>
    </form>
  )
}

function Newpassword({ tokenFromURL, actions, isAlert, alertActions }) {
  const {
    register,
    handleSubmit,
    errors,
    formState: { dirty, isSubmitting },
  } = useForm({ validationResolver: schemaValidate, validationContext: schemaNewpassword })
  const onSubmit = (data) => {
    actions(data)
  }
  const classes = ForgotPasswordStyles()
  const dataFromToken = jwtDecode(tokenFromURL)
  const latDate = parseInt(dataFromToken.iat + '000')
  const date2hours = moment(latDate).add(2, 'h').valueOf()
  const now = moment().valueOf()
  // console.log('Newpassword', { latDate, date2hours })

  return now > date2hours ? (
    <div>Token Expire</div>
  ) : (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className={classes.titleRoot}>
        <span
          className={classes.title}
        >{`Please enter new password for ${dataFromToken.EMAIL}`}</span>
      </div>
      <div className={classes.alertRoot}>
        <Collapse in={isAlert.open}>
          <Alert
            classes={{ filledError: classes.alert, icon: classes.alertIcon }}
            severity={'error'}
            onClose={alertActions.handleClose}
          >
            {isAlert.text}
          </Alert>
        </Collapse>
      </div>
      <div className={classes.inputRoot}>
        <Input
          id="password-input"
          name="PASSWORD"
          label="Password"
          type="password"
          className={classes.margin}
          inputRef={register}
          error={errors.PASSWORD}
          errorText={errors.PASSWORD && errors.PASSWORD.message}
          fullWidth
          outlined
        />
      </div>
      <div className={classes.inputRoot}>
        <Input
          id="confirm-password-input"
          name="RE_PASSWORD"
          label="Confirm Password"
          type="password"
          className={classes.margin}
          inputRef={register}
          error={errors.RE_PASSWORD}
          errorText={errors.RE_PASSWORD && errors.RE_PASSWORD.message}
          fullWidth
          outlined
        />
      </div>
      <div className={classes.buttonRoot}>
        <Button type="submit" size="large" fullWidth disabled={!dirty}>
          Submit
        </Button>
      </div>
    </form>
  )
}

function SubmitSuccess({ text, title, onClose }) {
  const classes = SubmitSuccessStyles()

  return (
    <div className={classes.root}>
      <div style={{ marginBottom: 16 }}>
        <Avatar variant="rounded" className={classes.avatarRounded}>
          <CheckIcon style={{ fontSize: '2.5rem' }} />
        </Avatar>
      </div>
      <div style={{ marginBottom: 16 }}>
        <span className={classes.title}>{title}</span>
      </div>
      <div style={{ marginBottom: 30 }}>
        <span className={classes.detail}>{text}</span>
      </div>
      <div>
        <Button onClick={onClose} size="large">
          Done
        </Button>
      </div>
    </div>
  )
}

function ForgotPassword({ open, onClose, type, actions, tokenFromURL, isAlert, alertActions }) {
  const [submitSuccess, setSubmitSuccess] = useState({
    open: false,
    text: '',
    title: '',
  })

  const classes = ForgotPasswordStyles()
  const handleRequest = (data) => {
    actions.requestPassword(data).then((res) => {
      if (res.status) {
        setSubmitSuccess({
          open: true,
          text: `An email regarding your request will be sent to ${res.message.data.detail}`,
          title: 'Request Submitted',
        })
      }
    })
  }
  const handleResetPassword = (data) => {
    actions.resetPassword({ ...data, tokenFromURL }).then((res) => {
      if (res.status) {
        setSubmitSuccess({
          open: true,
          text: `Your password for ${res.message.data.EMAIL} has been successfully changed`,
          title: 'Change Successful',
        })
      }
    })
  }

  return (
    <Modal
      open={open}
      headerChildren={
        !submitSuccess.open && <IconRounded title="Forgot Password" icon={<LockOutlinedIcon />} />
      }
      onClose={onClose}
    >
      {submitSuccess.open ? (
        <SubmitSuccess text={submitSuccess.text} onClose={onClose} />
      ) : (
        <div className={classes.root}>
          {type === 'request' && <Request actions={handleRequest} />}
          {type === 'reset' && (
            <Newpassword
              tokenFromURL={tokenFromURL}
              isAlert={isAlert}
              alertActions={alertActions}
              actions={handleResetPassword}
            />
          )}
        </div>
      )}
    </Modal>
  )
}

ForgotPassword.defaultProps = {
  type: 'request',
}

const mapStateToProps = (state) => {
  return {
    isAlert: state.isAlert,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
    alertActions: bindActionCreators(AlertActions, dispatch),
  }
}

export default nextConnect(mapStateToProps, mapDispatchToProps)(ForgotPassword)
