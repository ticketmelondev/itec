import { makeStyles } from '@material-ui/core/styles'

export const SubmitSuccessStyles = makeStyles((theme) => ({
  root: {
    textAlign: 'center',
    width: 375,
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  avatarRounded: {
    color: '#fff',
    backgroundColor: theme.palette.primary.main,
    width: 50,
    height: 50,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  title: {
    fontWeight: '600',
    fontSize: '16px',
    lineHeight: '24px',
    textTransform: 'capitalize',
    color: theme.palette.primary.main,
  },
  detail: {
    fontSize: '14px',
    lineHeight: '20px',
    textAlign: 'center',
    color: '#4D4D4D',
  },
}))

const ForgotPasswordStyles = makeStyles((theme) => ({
  root: {
    width: 375,
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  titleRoot: {
    marginBottom: '24px',
    // width: '360px',
    textAlign: 'center',
  },
  title: {
    fontSize: '20px',
    lineHeight: '20px',
    color: '#4D4D4D',
  },
  inputRoot: {
    padding: '8px 0',
  },
  buttonRoot: {
    padding: '24px 0',
  },
  alert: {
    color: theme.palette.primary.main,
    backgroundColor: '#FDE4E5',
    boxShadow: 'none',
    borderRadius: '5px',
  },
  alertIcon: {
    alignItems: 'center',
  },
}))

export default ForgotPasswordStyles
