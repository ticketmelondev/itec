import { makeStyles } from '@material-ui/core/styles'

const PaymentStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh',
    backgroundColor: '#F8F7F7',
  },
  paper: {
    width: 375,
    background: '#FFFFFF',
    borderRadius: 5,
    padding: 21,
    [theme.breakpoints.down('xs')]: {
      paddingLeft: 44,
      paddingRight: 44,
    },
  },
  detail: {
    marginBottom: 12,
  },
  gridContainer: {
    padding: '6px 0',
  },
  firstText: {
    color: 'rgba(0, 0, 0, 0.5)',
    fontSize: '16px',
    lineHeight: '20px',
  },
  lastText: {
    color: 'rgba(0, 0, 0, 0.8)',
    fontSize: '16px',
    lineHeight: '20px',
  },
  option: {},
}))

export default PaymentStyles
