import { makeStyles } from '@material-ui/core/styles'

const ChannelStyles = makeStyles((theme) => ({
  root: {},
  formControlLabel: {
    margin: 0,
  },
  radioRoot: {
    color: '#CCCCCC',
    padding: 0,
    '&$checked': {
      color: theme.palette.primary.main,
    },
  },
  checked: { color: theme.palette.primary.main },
  controlRoot: {
    width: '100%',
    border: ({ active }) =>
      active ? `2px solid ${theme.palette.primary.main}` : '1px solid #CCCCCC',
    borderRadius: 5,
    marginBottom: 24,
    padding: 16,
  },
  controlHeader: {
    display: 'flex',
    alignItems: 'center',
    '& .title': {
      flexGrow: 1,
      fontWeight: 600,
      fontSize: '16px',
      lineHeight: '24px',
      letterSpacing: '0.02em',
      color: 'rgba(0, 0, 0, 0.8)',
    },
  },
}))

export default ChannelStyles
