import React, { useState } from 'react'
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'

import ChannelStyles from './ChannelStyles'

import { getStatic } from '@lib/static'

function RadioControl({ text, selected, children, value, ...rest }) {
  const active = value === selected
  const classes = ChannelStyles({ active })

  return (
    <div className={classes.controlRoot}>
      <div className={classes.controlHeader}>
        <span className="title">{text}</span>
        <Radio
          value={value}
          checkedIcon={<CheckCircleIcon />}
          className={classes.radioRoot}
          classes={{
            checked: classes.checked,
          }}
          {...rest}
        />
      </div>

      {children && children}
    </div>
  )
}

function Channel({ onChange, value }) {
  const [selectedValue, setSelectedValue] = useState(value ? value : 'bank_tran')

  const handleChange = (event) => {
    setSelectedValue(event.target.value)
    onChange && onChange(event.target.value)
  }
  const classes = ChannelStyles()

  return (
    <div>
      <RadioGroup aria-label="channel" name="channel" value={selectedValue} onChange={handleChange}>
        <FormControlLabel
          className={classes.formControlLabel}
          value="bank_tran"
          control={
            <RadioControl
              text="Bank Transfer"
              selected={selectedValue}
              children={
                <div style={{ marginTop: 14 }}>
                  <img
                    src={`${getStatic('static/images/prompt.png')}`}
                    alt="img"
                    style={{ width: 75 }}
                  />
                </div>
              }
            />
          }
        />
        <FormControlLabel
          className={classes.formControlLabel}
          value="2c2p"
          control={
            <RadioControl
              text="Cash, Credit/Debit"
              selected={selectedValue}
              children={
                <div style={{ marginTop: 14 }}>
                  <img
                    src={`${getStatic('static/images/we_accept.png')}`}
                    alt="img"
                    style={{ width: 279 }}
                  />
                </div>
              }
            />
          }
        />
      </RadioGroup>
    </div>
  )
}

export default Channel
