import React, { Fragment, useState, useEffect } from 'react'
import { bindActionCreators, compose } from 'redux'
import { nextConnect } from '@redux/store'
import * as Actions from '@actions/orderActions'
import { toNumber, calculatAmount12Digit } from '@util/dataFormate'
import { getAPIRouter } from '@config/APIConfig'
import { VERSION_2C2P, MERCHANT_ID, CURRENCY } from '@config/constants'
import moment from 'moment'

import Grid from '@material-ui/core/Grid'
import DescriptionIcon from '@material-ui/icons/Description'
import AttachMoneyIcon from '@material-ui/icons/AttachMoney'

import PaymentStyles from './PaymentStyled'
import Channel from './Channel'

import Button from '@components/Button'
import IconRounded from '@components/IconRounded'
import Icon from '@components/Icon'

import classNames from 'classnames'

function CourseDetail({ details }) {
  const { DETAIL, COURSE_DETAIL } = details
  const classes = PaymentStyles()

  return (
    <div className={classNames(classes.paper, classes.detail)}>
      <div style={{ marginBottom: 28 }}>
        <IconRounded
          title="Course Details"
          icon={<Icon style={{ fontSize: '1.5rem', overflow: 'unset', width: 17 }}>doc</Icon>}
        />
      </div>

      <div>
        <Grid container className={classes.gridContainer}>
          <Grid item xs={3}>
            <span className={classes.firstText}>Name</span>
          </Grid>
          <Grid item xs={9}>
            <span className={classes.lastText}>{COURSE_DETAIL.COURSE_NAME}</span>
          </Grid>
        </Grid>
        <Grid container className={classes.gridContainer}>
          <Grid item xs={3}>
            <span className={classes.firstText}>Session</span>
          </Grid>
          <Grid item xs={9}>
            <span className={classes.lastText}>{DETAIL.SESSION_QUANTITY}</span>
          </Grid>
        </Grid>
        <Grid container className={classes.gridContainer}>
          <Grid item xs={3}>
            <span className={classes.firstText}>Price</span>
          </Grid>
          <Grid item xs={9}>
            <span className={classes.lastText}>{`${toNumber(DETAIL.PRICE, '0,0')} THB`}</span>
          </Grid>
        </Grid>
      </div>
    </div>
  )
}

function Payment2c2p({ details, hashValue }) {
  const { DETAIL, COURSE_DETAIL } = details

  const timeStam = moment(DETAIL.CREATED_AT).valueOf()
  const payment_description = COURSE_DETAIL.COURSE_NAME
  // const order_id = timeStam.toString().substring(0, 12)
  const order_id = DETAIL.ORDER_SHOT_ID
  const user_defined_1 = DETAIL.ORDER_ID
  const amount = calculatAmount12Digit(DETAIL.PRICE)
  // console.log('DETAIL', DETAIL)

  return (
    <div>
      <form id="myform" method="post" action={getAPIRouter('PAYMENT_URL_2C2P')}>
        <input type="hidden" name="version" value={VERSION_2C2P} />
        <input type="hidden" name="merchant_id" value={MERCHANT_ID} />
        <input type="hidden" name="currency" value={CURRENCY} />
        <input type="hidden" name="result_url_1" value={getAPIRouter('RESULT_PAYMENT')} />
        <input type="hidden" name="hash_value" value={hashValue} />
        <input type="hidden" name="user_defined_1" value={user_defined_1} />
        <input type="hidden" name="payment_description" value={payment_description} readonly />
        <input type="hidden" name="order_id" value={order_id} readonly />
        <input type="hidden" name="amount" value={amount} readonly />
        <Button type="submit" color="primary" size="large" fullWidth>
          Proceed
        </Button>
      </form>
    </div>
  )
}

function PaymentOption({ details, hashValue, actions }) {
  const [selectedValue, setSelectedValue] = useState('bank_tran')
  const { DETAIL } = details
  const classes = PaymentStyles()
  const tomorrow = moment(DETAIL.CREATED_AT).add(DETAIL.ORDER_EXPIRATION_TIME, 's').valueOf()
  const now = moment().valueOf()

  useEffect(() => {
    actions.updateOrder(selectedValue)
  }, [selectedValue])

  return (
    <div className={classNames(classes.paper, classes.option)}>
      {now > tomorrow ? (
        <div>Order Expire</div>
      ) : (
        <Fragment>
          <div style={{ marginBottom: 28 }}>
            <IconRounded
              title="Payment Option"
              icon={
                <Icon style={{ fontSize: '1.5rem', overflow: 'unset', width: 13 }}>dollarSign</Icon>
              }
            />
          </div>
          <Channel value={selectedValue} onChange={setSelectedValue} />
          {selectedValue === '2c2p' ? (
            <Payment2c2p details={details} hashValue={hashValue} />
          ) : (
            <div style={{ textAlign: 'center' }}>
              <Button
                onClick={() => (window.location = `/bank-transfer?order=${DETAIL.ORDER_ID}`)}
                color="primary"
                size="large"
                fullWidth
              >
                Proceed
              </Button>
            </div>
          )}
        </Fragment>
      )}
    </div>
  )
}

function Payment({ orderDetails, hashValue, isLoading, actions }) {
  const classes = PaymentStyles()

  return (
    <div className={classes.root}>
      {isLoading ? (
        <div>Loading...</div>
      ) : (
        Object.keys(orderDetails).length > 0 && (
          <div>
            <CourseDetail details={orderDetails} />
            <PaymentOption hashValue={hashValue} details={orderDetails} actions={actions} />
          </div>
        )
      )}
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    orderDetails: state.order.orderDetails,
    hashValue: state.order.hashValue,
    isLoading: state.isLoading.getOrderLoading,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
  }
}

const enhancer = compose(nextConnect(mapStateToProps, mapDispatchToProps))

export default enhancer(Payment)
