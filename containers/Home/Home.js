import React from 'react'
import { bindActionCreators, compose } from 'redux'
import { nextConnect } from '@redux/store'

import classNames from 'classnames'

import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'

import Link from '@components/Link'

import HomeStyles, { useStyles } from './HomeStyles'
import { getStatic } from '@lib/static'

function MediaCard({ name, description, slug, image, id }) {
  const classes = useStyles()
  const cardClasses = classNames({
    [classes.root]: true,
    [classes.cardFirst]: id === 0,
    [classes.cardSecond]: id === 1,
    [classes.cardThire]: id === 4,
    [classes.cardForu]: id === 5,
  })

  return (
    <Link route="institute" params={{ slug: slug }} passHref>
      <Card className={cardClasses}>
        <CardActionArea className={classes.cardActionArea}>
          <CardMedia className={classes.media} image={image} title="Contemplative Reptile" />
          <CardContent className={classes.content}>
            <Typography
              className={classes.typographyTitle}
              gutterBottom
              variant="h5"
              component="h2"
            >
              {name}
            </Typography>
            <Typography
              className={classes.typographyDetail}
              variant="body2"
              color="textSecondary"
              component="p"
            >
              {description}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </Link>
  )
}

function Home({ institute }) {
  const classes = HomeStyles()
  // console.log('Home', institute)

  return (
    <div className={classes.root}>
      <div>
        <img
          src={`https://itec-public-file.s3-ap-southeast-1.amazonaws.com/banner/AW_EW+APP_Banner_FINAL.png`}
          alt="banner"
          style={{ width: '100%', height: '100%' }}
        />
      </div>
      <Container maxWidth="lg">
        <div className={classes.cardContainer}>
          <Grid className={classes.gridContainer} container>
            {institute.map((item, key) => (
              <Grid
                key={key}
                style={{ borderBottom: (key === 4 && 'none') || (key === 5 && 'none') }}
                className={classes.gridItem}
                item
                xs={12}
                sm={6}
              >
                <MediaCard
                  id={key}
                  name={item.NAME}
                  description={item.DESC}
                  image={item.IMG}
                  slug={item.SLUG}
                />
              </Grid>
            ))}
          </Grid>
        </div>
      </Container>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    institute: state.courses.institute,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {}
}

export default nextConnect(mapStateToProps, mapDispatchToProps)(Home)
