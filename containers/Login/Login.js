import React from 'react'
import { bindActionCreators } from 'redux'
import { nextConnect } from '@redux/store'
import * as Actions from '@actions/registerActions'
import * as AlertActions from '@actions/alertActions'
import { useForm } from 'react-hook-form'
import * as yup from 'yup'
import classNames from 'classnames'

import Typography from '@material-ui/core/Typography'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'
import Collapse from '@material-ui/core/Collapse'
import CircularProgress from '@material-ui/core/CircularProgress'

import Input from '@components/Input'
import Button from '@components/Button'
import Icon from '@components/Icon'
import { Alert } from '@components/Snackbar'

import LoginStyles from './LoginStyles'

const schema = yup.object().shape({
  EMAIL: yup.string().email().required(),
  PASSWORD: yup.string().required(),
})

function Login({ actions, setStep, isAlert, alertActions, isLoading }) {
  const { register, handleSubmit, errors } = useForm({
    validationSchema: schema,
  })
  const onSubmit = (data) => {
    actions.auth(data)
  }

  const classes = LoginStyles()

  return (
    <div className={classes.root}>
      <div className={classes.titleRoot}>
        <Typography className={classes.title} variant="h4">
          Log In to your Account
        </Typography>
      </div>
      <div className={classes.alertRoot}>
        <Collapse in={isAlert.open}>
          <Alert
            classes={{ filledError: classes.alert, icon: classes.alertIcon }}
            severity={'error'}
            onClose={alertActions.handleClose}
          >
            {isAlert.text}
          </Alert>
        </Collapse>
      </div>

      <form onSubmit={handleSubmit(onSubmit)}>
        <div className={classes.inputRoot}>
          <Input
            id="email-input"
            name="EMAIL"
            label="Email"
            className={classes.margin}
            inputRef={register}
            error={errors.EMAIL}
            errorText={errors.EMAIL && errors.EMAIL.message}
          />
        </div>
        <div className={classes.inputRoot}>
          <Input
            id="password-input"
            name="PASSWORD"
            label="Password"
            type="password"
            className={classes.margin}
            inputRef={register}
            error={errors.PASSWORD}
            errorText={errors.PASSWORD && errors.PASSWORD.message}
          />
        </div>
        <div className={classNames(classes.inputRoot, classes.inputRootDisplayFlex)}>
          <div className="remember">
            <FormControlLabel
              className={classes.formControlLabel}
              classes={{
                label: classes.labelCheck,
              }}
              control={
                <Checkbox
                  name="REMEMBER"
                  inputRef={register}
                  className={classes.radioRoot}
                  classes={{
                    checked: classes.checked,
                  }}
                  icon={<Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>uncheck</Icon>}
                  checkedIcon={
                    <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>checked</Icon>
                  }
                />
              }
              label="Remember me"
            />
          </div>
          <div className="forgot" onClick={() => setStep('forgot')}>
            <span>Forgot Password?</span>
          </div>
        </div>
        <div className={classes.buttonRoot}>
          <Button type="submit" color="primary" size="large" disabled={isLoading}>
            Log In
          </Button>
          {isLoading && <CircularProgress size={24} className={classes.buttonProgress} />}
        </div>
      </form>

      <div className={classes.accountRoot} onClick={() => setStep('register')}>
        <span className={classes.account}>Create an Account</span>
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    isAlert: state.isAlert,
    isLoading: state.isLoading.accessAccount,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
    alertActions: bindActionCreators(AlertActions, dispatch),
  }
}

export default nextConnect(mapStateToProps, mapDispatchToProps)(Login)
