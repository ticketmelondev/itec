import { makeStyles } from '@material-ui/core/styles'

const RegisterStyles = makeStyles((theme) => ({
  root: {
    width: '854px',
    // height: '642px',
    height: ({ height }) => (height ? height : '100%'),
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      height: '100%',
    },
  },
  titleRoot: {
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: '24px',
    width: '360px',
    textAlign: 'center',
  },
  title: {
    fontSize: '2.5714rem',
    lineHeight: '44px',
    color: 'rgba(0, 0, 0, 0.7)',
  },
  inputRoot: {
    marginBottom: 16,
  },
  buttonRoot: {
    marginBottom: 24,
    textAlign: 'center',
    position: 'relative',
  },
  buttonProgress: {
    color: theme.palette.primary.main,
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
  firstFormRoot: {
    textAlign: 'center',
    width: 702,
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  secondFormRoot: {
    width: 702,
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  gridFirst: {
    // textAlign: 'right',
  },
  personal: {
    fontWeight: 600,
    fontSize: '16px',
    lineHeight: '24px',
    color: 'rgba(0, 0, 0, 0.7)',
    marginBottom: 8,
  },
  accountRoot: {
    textAlign: 'center',
    cursor: 'pointer',
  },
  account: {
    color: theme.palette.primary.main,
    fontSize: '18px',
    fontWeight: 600,
    lineHeight: '28px',
  },
  formControlLabel: {
    margin: 0,
  },
  radioRoot: {
    color: '#CCCCCC',
    padding: 0,
    marginRight: 8,
    '&$checked': {
      color: theme.palette.primary.main,
    },
  },
  checked: { color: theme.palette.primary.main },
  labelCheck: {
    fontSize: '14px',
    lineHeight: '20px',
    letterSpacing: '0.02em',
    color: 'rgba(0, 0, 0, 0.56)',
  },
  terms: {
    fontSize: '14px',
    lineHeight: '20px',
    letterSpacing: '0.02em',
    color: theme.palette.primary.main,
    cursor: 'pointer',
    display: 'inline-flex',
    alignItems: 'center',
    verticalAlign: 'middle',
    '-webkit-tap-highlight-color': 'transparent',
  },
  alertRoot: {
    marginBottom: '24px',
    width: '360px',
  },
  alert: {
    color: theme.palette.primary.main,
    backgroundColor: '#FDE4E5',
    boxShadow: 'none',
    borderRadius: '5px',
  },
  alertIcon: {
    alignItems: 'center',
  },
}))

export default RegisterStyles
