import React, { Fragment } from 'react'
import { useForm, Controller } from 'react-hook-form'

import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import CircularProgress from '@material-ui/core/CircularProgress'

import Input from '@components/Input'
import Button from '@components/Button'
import InputDate from '@components/InputDate'
import SelectDob from '@components/SelectDob'

import RegisterStyles from './RegisterStyles'
import { schemaSecondForm, schemaValidate, validateAge } from './validate'

function ContactPersonal({ register, errors }) {
  const classes = RegisterStyles()

  return (
    <Fragment>
      <Typography className={classes.personal} variant="h5">
        Contact Person
      </Typography>
      <Grid className={classes.personal} container spacing={2}>
        <Grid className={classes.gridFirst} item xs={12} sm={6} md={6}>
          <Input
            id="firstname-person-input"
            name="CP_FISRT_NAME"
            label="First Name"
            className={classes.margin}
            inputRef={register}
            error={errors.CP_FISRT_NAME}
            errorText={errors.CP_FISRT_NAME && errors.CP_FISRT_NAME.message}
            fullWidth
          />
        </Grid>
        <Grid item xs={12} sm={6} md={6}>
          <Input
            id="lastname-person-input"
            name="CP_LAST_NAME"
            label="Last Name"
            className={classes.margin}
            inputRef={register}
            error={errors.CP_LAST_NAME}
            errorText={errors.CP_LAST_NAME && errors.CP_LAST_NAME.message}
            fullWidth
          />
        </Grid>
        <Grid className={classes.gridFirst} item xs={12} sm={6} md={6}>
          <Input
            id="relationship-input"
            name="RELATIONSHIP"
            label="Relationship"
            className={classes.margin}
            inputRef={register}
            error={errors.RELATIONSHIP}
            errorText={errors.RELATIONSHIP && errors.RELATIONSHIP.message}
            fullWidth
          />
        </Grid>
        <Grid item xs={12} sm={6} md={6}>
          <Input
            id="phone-person-input"
            name="CP_CONTACT_NO"
            label="Phone Number"
            className={classes.margin}
            inputRef={register}
            error={errors.CP_CONTACT_NO}
            errorText={errors.CP_CONTACT_NO && errors.CP_CONTACT_NO.message}
            fullWidth
          />
        </Grid>
        <Grid className={classes.gridFirst} item xs={12} sm={6} md={6}>
          <Input
            id="email-person-input"
            name="CP_EMAIL"
            label="Email (Optional)"
            className={classes.margin}
            inputRef={register}
            error={errors.CP_EMAIL}
            errorText={errors.CP_EMAIL && errors.CP_EMAIL.message}
            fullWidth
          />
        </Grid>
        <Grid item xs={12} sm={6} md={6}>
          <Input
            id="line-person-input"
            name="CP_LINE"
            label="Line (Optional)"
            className={classes.margin}
            inputRef={register}
            error={errors.CP_LINE}
            errorText={errors.CP_LINE && errors.CP_LINE.message}
            fullWidth
          />
        </Grid>
      </Grid>
    </Fragment>
  )
}

function PersonalInfo({ register, errors, Controllers, control }) {
  const classes = RegisterStyles()

  return (
    <Fragment>
      <Grid className={classes.inputRoot} container spacing={2}>
        <Grid className={classes.gridFirst} item xs={12} sm={6} md={6}>
          <Input
            id="firstname-input"
            name="FISRT_NAME"
            label="First Name"
            className={classes.margin}
            inputRef={register}
            error={errors.FISRT_NAME}
            errorText={errors.FISRT_NAME && errors.FISRT_NAME.message}
            fullWidth
          />
        </Grid>
        <Grid item xs={12} sm={6} md={6}>
          <Input
            id="lastname-input"
            name="LAST_NAME"
            label="Last Name"
            className={classes.margin}
            inputRef={register}
            error={errors.LAST_NAME}
            errorText={errors.LAST_NAME && errors.LAST_NAME.message}
            fullWidth
          />
        </Grid>
        <Grid className={classes.gridFirst} item xs={12} sm={6} md={6}>
          <Input
            id="nickname-input"
            name="NICK_NAME"
            label="Nickname"
            className={classes.margin}
            inputRef={register}
            error={errors.NICK_NAME}
            errorText={errors.NICK_NAME && errors.NICK_NAME.message}
            fullWidth
          />
        </Grid>
        <Grid item xs={12} sm={6} md={6}>
          <Controllers
            as={
              <SelectDob
                id="date-input"
                name="DOB"
                error={errors.DOB}
                errorText={errors.DOB && errors.DOB.message}
              />
            }
            name="DOB"
            control={control}
          />
          {/* <Controllers
            as={
              <InputDate
                id="date-input"
                name="DOB"
                label="Date Of Birth"
                className={classes.margin}
                inputRef={register}
                error={errors.DOB}
                errorText={errors.DOB && errors.DOB.message}
                fullWidth
              />
            }
            name="DOB"
            control={control}
          /> */}
        </Grid>
        <Grid className={classes.gridFirst} item xs={12} sm={6} md={6}>
          <Input
            id="phone-input"
            name="CONTACT_NO"
            label="Phone Number"
            className={classes.margin}
            inputRef={register}
            error={errors.CONTACT_NO}
            errorText={errors.CONTACT_NO && errors.CONTACT_NO.message}
            fullWidth
          />
        </Grid>
        <Grid item xs={12} sm={6} md={6}>
          <Input
            id="line-input"
            name="LINE"
            label="Line ID (Optional)"
            className={classes.margin}
            inputRef={register}
            error={errors.LINE}
            errorText={errors.LINE && errors.LINE.message}
            fullWidth
          />
        </Grid>
        <Grid className={classes.gridFirst} item xs={12} sm={6} md={6}>
          <Input
            id="select-input"
            name="SC_NAME"
            label="School / Company Name (Optional)"
            className={classes.margin}
            inputRef={register}
            error={errors.SC_NAME}
            errorText={errors.SC_NAME && errors.SC_NAME.message}
            fullWidth
          />
        </Grid>
      </Grid>
    </Fragment>
  )
}

function SecondForm({ actions, isLoading }) {
  const { register, handleSubmit, errors, watch, control } = useForm({
    validationResolver: schemaValidate,
    validationContext: schemaSecondForm,
  })
  const onSubmit = async (data) => {
    // console.log('onSubmit', { data })
    await actions.setRegisterDetails(data)
    actions.rgisterAccount()
  }
  // console.log('errors', errors)

  const classes = RegisterStyles()
  const watchDateOfBirth = watch('DOB')
  const now = validateAge(watchDateOfBirth)

  return (
    <div className={classes.secondFormRoot}>
      <Typography className={classes.personal} variant="h5">
        Personal Info
      </Typography>
      <form onSubmit={handleSubmit(onSubmit)}>
        <PersonalInfo
          register={register}
          errors={errors}
          Controllers={Controller}
          control={control}
        />
        {now < 15 && <ContactPersonal register={register} errors={errors} />}
        <div className={classes.buttonRoot}>
          <Button type="submit" color="primary" size="large" disabled={isLoading}>
            Register
          </Button>
          {isLoading && <CircularProgress size={24} className={classes.buttonProgress} />}
        </div>
      </form>
    </div>
  )
}

export default SecondForm
