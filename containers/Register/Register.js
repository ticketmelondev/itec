import React from 'react'
import { bindActionCreators } from 'redux'
import { nextConnect } from '@redux/store'
import * as Actions from '@actions/registerActions'
import * as AlertActions from '@actions/alertActions'

import Typography from '@material-ui/core/Typography'
import Collapse from '@material-ui/core/Collapse'

import { Alert } from '@components/Snackbar'

import RegisterStyles from './RegisterStyles'
import FirstForm from './FirstForm'
import SecondForm from './SecondForm'

function Register({ register, actions, setStep, isAlert, alertActions, isLoading }) {
  const { page } = register
  const height = page === 1 && 642
  const classes = RegisterStyles({ height })
  // console.log('register', { register })

  return (
    <div className={classes.root}>
      <div className={classes.titleRoot}>
        <Typography className={classes.title} variant="h4">
          Create an account
        </Typography>
      </div>
      <div className={classes.alertRoot}>
        <Collapse in={isAlert.open}>
          <Alert
            classes={{ filledError: classes.alert, icon: classes.alertIcon }}
            severity={'error'}
            onClose={alertActions.handleClose}
          >
            {isAlert.text}
          </Alert>
        </Collapse>
      </div>

      {page === 1 && <FirstForm actions={actions} />}
      {page === 2 && <SecondForm actions={actions} isLoading={isLoading} />}

      <div className={classes.accountRoot} onClick={() => setStep('login')}>
        <span className={classes.account}>Already have an account?</span>
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    register: state.register,
    isAlert: state.isAlert,
    isLoading: state.isLoading.accessAccount,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
    alertActions: bindActionCreators(AlertActions, dispatch),
  }
}

export default nextConnect(mapStateToProps, mapDispatchToProps)(Register)
