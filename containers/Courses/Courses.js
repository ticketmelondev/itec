import React from 'react'
import { bindActionCreators, compose } from 'redux'
import { nextConnect } from '@redux/store'
import LazyLoad from 'react-lazyload'

import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import Skeleton from '@material-ui/lab/Skeleton'

import Card from '@components/Card'
import TextUnderline from '@components/TextUnderline'
import CourseCard from '@components/CourseCard'

import CoursesStyles from './CoursesStyled'

function PlaceholderLoading() {
  return (
    <div>
      <Skeleton variant="rect" height={202} />
      <Skeleton variant="text" />
      <Skeleton variant="text" width="60%" />
    </div>
  )
}

function LazyLoads({ item, key }) {
  return (
    <LazyLoad height={200} placeholder={<PlaceholderLoading />} debounce={200}>
      <CourseCard
        title={item.DETAIL.COURSE_NAME}
        detail={item.DETAIL.DETAIL}
        levels={item.LEVEL}
        page="course"
        slugId={item.DETAIL.COURSE_ID}
        image={item.DETAIL.COURSE_IMG}
      />
    </LazyLoad>
  )
}

function Courses({ allCourses, instituteDetail }) {
  // console.log('allCourses', { allCourses })
  const classes = CoursesStyles()

  return (
    <div className={classes.root}>
      <Container maxWidth="lg">
        <div className={classes.padding}>
          {instituteDetail.NAME && (
            <TextUnderline>{`${instituteDetail.NAME} Courses`}</TextUnderline>
          )}
        </div>

        <div className={classes.padding}>
          <Grid container spacing={1}>
            {allCourses.length > 0 &&
              allCourses.map(
                (item, key) =>
                  !item.DETAIL.HIDE && (
                    <Grid key={key} item xs={12} sm={4} md={4} lg={4} className="card">
                      <LazyLoads item={item} key={key} />
                    </Grid>
                  ),
              )}
          </Grid>
        </div>
      </Container>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    allCourses: state.courses.allCourses,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {}
}

export default nextConnect(mapStateToProps, mapDispatchToProps)(Courses)
