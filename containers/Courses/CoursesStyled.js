import { makeStyles } from '@material-ui/core/styles'

const CoursesStyles = makeStyles((theme) => ({
  root: {
    marginBottom: 85,
    '& .card': {
      marginBottom: 24,
    },
    '& .card:last-child': {
      marginBottom: 0,
    },
  },
  padding: {
    padding: 0,
    [theme.breakpoints.up('lg')]: {
      padding: '0 64px',
    },
  },
}))

export default CoursesStyles
