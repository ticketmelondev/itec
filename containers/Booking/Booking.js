import React, { useState } from 'react'
import dynamic from 'next/dynamic'
import { bindActionCreators } from 'redux'
import { nextConnect } from '@redux/store'
import * as Actions from '@actions/bookingActions'
import * as AlertActions from '@actions/alertActions'
import { useForm, Controller } from 'react-hook-form'
import { bookingType, sessionNumber, dayOfClass, startDate, timePeriod } from '@util/bookingFunc'
import { toNumber, toTimeFormat, getHours, getMinutes } from '@util/dataFormate'

import Description from '@material-ui/icons/Description'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Collapse from '@material-ui/core/Collapse'

import IconRounded from '@components/IconRounded'
import Autocomplete from '@components/Autocomplete'
import Button from '@components/Button'
import Modal from '@components/Modal'
import { Alert } from '@components/Snackbar'

import BookingStyles from './BookingStyles'
import {
  CustomInputType,
  CustomInputDate,
  CustomInputTime,
  CustomInputSession,
  CustomInputTimeH,
  CustomInputTimeM,
} from './Field'

import moment from 'moment'

const BackdropLoading = dynamic(() => import('@components/BackdropLoading'), {
  ssr: false,
  loading: () => null,
})

const displayTime = (sessionTime) => {
  const h = Math.floor(sessionTime / 3600)
  const m = Math.floor((sessionTime % 3600) / 60)
  const s = Math.floor((sessionTime % 3600) % 60)
  // console.log('displayTime', { h, m, s })
  if (h > 0) return `${h} Hours`
  else if (m > 0) return `${m} Minute`
  return
}

function Booking({
  sessionDetail,
  open,
  onClose,
  actions,
  orderIsLoading,
  isLoading,
  isAlert,
  alertActions,
}) {
  const [period, setPeriod] = useState([])
  // const [isLoading, setIsLoading] = useState(false)
  const [state, setState] = useState({
    type: '',
    session: 1,
    date: '',
    time_hour: '',
    time_min: '',
  })
  // console.log('sessionDetail', { sessionDetail, isAlert })

  const { SESSIONS, COURSE_DETAIL, SESSION_TIME, SESSION_QUANTITY } = sessionDetail
  // console.log('SESSIONS', SESSIONS)
  const sessions = SESSIONS ? SESSIONS.filter((i) => i.S_STATUS !== 'INACTIVE') : []
  const classes = BookingStyles()

  const onChange = async (value, name) => {
    let data = {}
    if (name === 'type') {
      data = {
        ...state,
        [name]: value,
        session: { label: '1', value: 1 },
        date: '',
        time_hour: '',
        time_min: '',
      }
    } else if (name === 'date') {
      data = {
        ...state,
        [name]: value,
        time_hour: '',
        time_min: '',
      }
    } else if (name === 'time_hour') {
      data = {
        ...state,
        [name]: value,
      }
    } else if (name === 'time_min') {
      data = {
        ...state,
        [name]: value,
      }
    } else if (name === 'session') {
      data = {
        ...state,
        [name]: value,
      }
    }
    setState(data)
  }
  const handleClose = () => {
    setState({
      type: '',
      session: 1,
      date: '',
      class_time: '',
    })
    onClose()
  }

  const handleCreateSession = () => {
    actions.createSession(state).then((res) => {
      if (res.status) {
        setState({
          type: '',
          session: 1,
          date: '',
          time_hour: '',
          time_min: '',
        })
      }
    })
  }

  return (
    <Modal
      open={open}
      onClose={handleClose}
      headerChildren={<IconRounded title="Booking" icon={<Description />} />}
    >
      {Object.keys(sessionDetail).length > 0 && (
        <div className={classes.root}>
          <div>
            <Grid container className={classes.gridContainer}>
              <Grid item xs={3}>
                <span className={classes.firstText}>Course</span>
              </Grid>
              <Grid item xs={9}>
                <span className={classes.lastText}>{COURSE_DETAIL.DETAIL.COURSE_NAME}</span>
              </Grid>
            </Grid>
            <Grid container className={classes.gridContainer}>
              <Grid item xs={3}>
                <span className={classes.firstText}>Class Duration</span>
              </Grid>
              <Grid item xs={9}>
                <span className={classes.lastText}>{displayTime(SESSION_TIME)}</span>
              </Grid>
            </Grid>
            <Grid container className={classes.gridContainer}>
              <Grid item xs={3}>
                <span className={classes.firstText}>Session</span>
              </Grid>
              <Grid item xs={9}>
                <span className={classes.lastText}>{`${sessions.length}/${SESSION_QUANTITY}`}</span>
              </Grid>
            </Grid>
          </div>

          <div className={classes.form}>
            {/* <Autocomplete loading={true} options={optionsFetch} /> */}
            <div className={classes.inputRoot}>
              <CustomInputType
                label="Booking Type"
                name="type"
                onChange={onChange}
                value={state.type}
                options={bookingType}
                fullWidth
              />
            </div>
            {state.type && ['daily', 'weekly'].includes(state.type.value) && (
              <div className={classes.inputRoot}>
                <CustomInputSession
                  label="Number of session"
                  name="session"
                  onChange={onChange}
                  value={state.session}
                  options={sessionNumber(SESSION_QUANTITY, SESSIONS.length)}
                  fullWidth
                />
              </div>
            )}
            <div className={classes.inputRoot}>
              <span>Please select Date and Time</span>
            </div>
            <div className={classes.inputRoot}>
              <CustomInputDate
                label="Date"
                name="date"
                onChange={onChange}
                value={state.date}
                disabled={!state.type}
                outlined
                fullWidth
              />
            </div>

            <div className={classes.inputRoot}>
              <div className={classes.boxInputTime}>
                <div className={classes.labelTime}>Class Time</div>
                <Grid container>
                  <Grid items xs={6}>
                    <CustomInputTimeH
                      label="HH"
                      name="time_hour"
                      onChange={onChange}
                      value={state.time_hour}
                      options={getHours()}
                      disabled={!state.date}
                      outlined
                      fullWidth
                    />
                  </Grid>
                  <Grid items xs={6}>
                    <CustomInputTimeM
                      label="MM"
                      name="time_min"
                      onChange={onChange}
                      value={state.time_min}
                      options={getMinutes()}
                      disabled={!state.date}
                      outlined
                      fullWidth
                    />
                  </Grid>
                </Grid>
              </div>
            </div>

            {/* <div className={classes.inputRoot}>
              <CustomInputTime
                label="Class Time"
                name="class_time"
                onChange={onChange}
                value={state.class_time}
                options={toTimeFormat()}
                disabled={!state.date}
                outlined
                fullWidth
              />
            </div> */}
            <div className={classes.alertRoot}>
              {isAlert.open && isAlert.type === 'error' && (
                <Collapse in={isAlert.open}>
                  <Alert
                    classes={{ filledError: classes.alert, icon: classes.alertIcon }}
                    severity={'error'}
                    onClose={alertActions.handleClose}
                  >
                    {`ไม่สามารถเริ่มจอง ${moment(isAlert.text[0]).format('D MMM YYYY HH:mm')} ได้`}
                  </Alert>
                </Collapse>
              )}
            </div>
            <div style={{ color: 'rgba(0, 0, 0, 0.5)' }}>
              <span>Notes: Reminder notices are sent 1 hour before the course starts</span>
            </div>
            <div className={classes.buttonRoot}>
              <Button
                color="primary"
                size="large"
                fullWidth
                disabled={!state.time_hour || !state.time_min}
                onClick={handleCreateSession}
              >
                Book Now
              </Button>
            </div>
          </div>
        </div>
      )}

      {isLoading && <BackdropLoading open={isLoading} />}
    </Modal>
  )
}

const mapStateToProps = (state) => {
  return {
    bookingDetail: state.booking.bookingDetail,
    orderIsLoading: state.isLoading.orderLoading,
    isAlert: state.isAlert,
    isLoading: state.isLoading.createSessionLoading,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
    alertActions: bindActionCreators(AlertActions, dispatch),
  }
}
export default nextConnect(mapStateToProps, mapDispatchToProps)(Booking)
