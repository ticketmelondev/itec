import { makeStyles } from '@material-ui/core/styles'

const BookingStyles = makeStyles((theme) => ({
  root: {
    width: 375,
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  titleRoot: {
    marginBottom: 32,
  },
  title: {
    textTransform: 'capitalize',
    color: 'rgba(0, 0, 0, 0.8)',
    '& h5': {
      fontSize: '20px',
      lineHeight: '120%',
    },
  },
  titleSecond: {
    textTransform: 'capitalize',
    '& span': {
      fontSize: '14px',
      lineHeight: '120%',
      color: '#7A7A7A',
    },
  },
  form: {},
  inputRoot: {
    padding: '8px 0',
  },
  buttonRoot: {
    padding: '24px 0',
  },
  gridContainer: {
    padding: '6px 0',
  },
  firstText: {
    color: 'rgba(0, 0, 0, 0.5)',
    fontSize: '16px',
    lineHeight: '20px',
  },
  lastText: {
    color: 'rgba(0, 0, 0, 0.8)',
    fontSize: '16px',
    lineHeight: '20px',
  },
  alertRoot: {
    marginBottom: '24px',
    // width: '360px',
  },
  alert: {
    color: theme.palette.primary.main,
    backgroundColor: '#FDE4E5',
    boxShadow: 'none',
    borderRadius: '5px',
  },
  alertIcon: {
    alignItems: 'center',
  },
  boxInputTime: {
    border: '1px solid #CCC',
    borderRadius: '5px',
  },
  labelTime: {
    padding: '4px 15px',
    fontSize: '12px',
    lineHeight: '16px',
    color: '#808080',
  },
}))

export default BookingStyles
