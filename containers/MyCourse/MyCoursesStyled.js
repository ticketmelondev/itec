import { makeStyles } from '@material-ui/core/styles'

const MyCoursesStyles = makeStyles((theme) => ({
  root: {
    paddingTop: '56px',
    paddingBottom: '80px',
    backgroundColor: '#E5E5E5',
  },
  TypographyH4: {
    fontWeight: 'normal',
    fontSize: '2rem',
    lineHeight: 2.375,
    color: 'rgba(0, 0, 0, 0.7)',
  },
  TypographyH5: {
    fontSize: 24,
    fontWeight: 'normal',
    lineHeight: 1.8125,
    color: '#000000',
    paddingBottom: 16,
  },
  TypographySpan: {
    fontSize: 14,
    fontWeight: 'bold',
    lineHeight: 1.8125,
    color: '#ED1B24',
    paddingBottom: 16,
  },
  rootAlert: {
    padding: '8px',
    borderRadius: '5px',
    fontWeight: '600',
    fontSize: '0.875rem',
    lineHeight: '1.0625rem',
    letterSpacing: '0.02em',
  },
  standardError: {
    color: theme.palette.primary.main,
    backgroundColor: '#FDE8E9',
  },
  iconAlert: {
    padding: 0,
    opacity: 1,
    fontSize: '1.125rem',
  },
  iconBoxAlert: {
    backgroundColor: theme.palette.primary.main,
    borderRadius: '4px',
    color: 'inherit',
    width: '28px',
    height: '28px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconStyles: {
    color: '#FFF',
    fontWeight: '600!important',
  },
  actionAlert: {
    marginRight: 0,
    fontWeight: 'bold',
  },
  sectionTitle: {
    borderLeft: `4px solid ${theme.palette.primary.main}`,
    paddingLeft: 10,
    fontSize: 18,
    fontWeight: 600,
    lineHeight: '1.75rem',
    color: 'rgba(0, 0, 0, 0.8)',
  },
  rootScheduleDetails: {
    marginTop: '16px',
  },
  scheduleBox: {
    height: '405px',
    padding: '24px',
    background: '#FFFFFF',
    borderRadius: '5px',
    boxShadow: '0px 1px 4px rgba(0, 0, 0, 0.12)',
    overflow: 'hidden',
  },
  sectionScheduleDetails: {
    display: 'flex',
    paddingBottom: '12px',
    fontSize: '0.875rem',
    lineHeight: '1.5rem',
    '& .time': {
      padding: '12px 16px',
      color: 'rgba(0, 0, 0, 0.6)',
    },
  },
  sectionDetails: {
    width: '100%',
    background: '#F8F7F7',
    borderRadius: '4px',
    padding: '12px 16px',
    '& .title': {
      fontWeight: 600,
      color: 'rgba(0, 0, 0, 0.7)',
    },
    '& .alert': {
      backgroundColor: '#FDE8E9',
      borderRadius: '5px',
      color: theme.palette.primary.main,
      padding: '2px 8px',
      marginLeft: '8px',
    },
    '& .description': {
      lineHeight: '1.25rem',
      color: 'rgba(0, 0, 0, 0.5)',
    },
  },
  rootCourseLists: {
    marginTop: '40px',
  },
}))

export default MyCoursesStyles
