import { makeStyles } from '@material-ui/core/styles'

export const ImportantStyles = makeStyles((theme) => ({
  root: {
    width: 375,
    borderTop: '1px solid #E1E1E1',
    paddingTop: 25,
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  detailTitle: {
    fontSize: 14,
    lineHeight: '20px',
    color: '#4D4D4D',
    marginBottom: 8,
    '& .span1': {
      fontWeight: 600,
    },
  },
  detailList: {
    '& ul': {
      paddingLeft: 16,
      fontSize: 14,
      lineHeight: '20px',
      color: '#4D4D4D',
    },
  },
  detailContact: {
    fontSize: 14,
    lineHeight: '20px',
    color: '#4D4D4D',
  },
}))

export const MakePaymentStyles = makeStyles((theme) => ({
  root: {
    width: 375,
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  gridContainer: {
    padding: '6px 0',
  },
  firstText: {
    color: 'rgba(0, 0, 0, 0.5)',
    fontSize: '16px',
    lineHeight: '20px',
  },
  lastText: {
    color: 'rgba(0, 0, 0, 0.8)',
    fontSize: '16px',
    lineHeight: '20px',
  },
  warningDetail: {
    textAlign: 'center',
    background: '#F8F7F7',
    borderRadius: '5px',
    fontSize: 13,
    lineHeight: '16px',
    color: theme.palette.primary.main,
    padding: '8px 28px',
    marginBottom: 23,
  },
}))

const PandingStyles = makeStyles((theme) => ({
  root: {
    background: '#FFFFFF',
    boxShadow: 'inset 0px -1px 0px rgba(0, 0, 0, 0.06)',
    borderRadius: '5px',
    borderLeft: `4px solid ${theme.palette.primary.main}`,
    marginBottom: 16,
    padding: '16px 24px',
  },
  statusPanding: {
    width: 163,
    background: '#FFEBBA',
    borderRadius: 5,
    padding: '4px 13px',
    fontWeight: 600,
    fontSize: 13,
    lineHeight: '20px',
    textAlign: 'center',
    color: '#83610D',
    cursor: 'pointer',
    marginLeft: 'auto',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  span: {
    fontSize: 14,
    lineHeight: '20px',
    color: 'rgba(0, 0, 0, 0.6)',
  },
}))

export default PandingStyles
