import React, { Fragment, useState, useEffect } from 'react'
import { bindActionCreators, compose } from 'redux'
import { nextConnect } from '@redux/store'
import * as ModalActions from '@actions/modalActions'
import * as Actions from '@actions/courseActions'
import moment from 'moment'
import {
  toDateTimeFormat,
  displayTime,
  toTimeFormat,
  getHours,
  getMinutes,
} from '@util/dataFormate'
import { bookandrescheduleflow } from '@util/myCourseFunc'

import Grid from '@material-ui/core/Grid'
import Avatar from '@material-ui/core/Avatar'
import AssignmentIcon from '@material-ui/icons/Assignment'
import Collapse from '@material-ui/core/Collapse'
import CloseIcon from '@material-ui/icons/Close'
import DoneIcon from '@material-ui/icons/Done'
import WatchIcon from '@material-ui/icons/Watch'
import Typography from '@material-ui/core/Typography'

import Modal from '@components/Modal'
import Icon from '@components/Icon'
import IconRounded from '@components/IconRounded'
import Button from '@components/Button'
import Input from '@components/Input'
import { Alert } from '@components/Snackbar'

import { useMember } from '@containers/auth'

import ViewClassStyles from './ViewClassStyles'
import {
  CustomInputType,
  CustomInputDate,
  CustomInputTime,
  CustomInputSession,
  CustomInputTimeH,
  CustomInputTimeM,
} from './Field'

function StatusComponent({ details }) {
  const classes = ViewClassStyles()

  if (details.S_STATUS === 'WAITING_APPROVE') {
    return (
      <Grid item>
        <div className={classes.statusPanding}>
          {/* <Icon style={{ fontSize: '1.5rem', overflow: 'unset', marginRight: 4 }}>clock</Icon> */}
          <WatchIcon />
          Pending
        </div>
      </Grid>
    )
  } else if (details.S_STATUS === 'ACTIVE') {
    if (details.STATUS_NEW_START_TIME && details.STATUS_NEW_START_TIME === 'req') {
      return (
        <Grid item>
          <div className={classes.statusPanding}>
            {/* <Icon style={{ fontSize: '1.5rem', overflow: 'unset', marginRight: 4 }}>clock</Icon> */}
            <WatchIcon />
            Pending
          </div>
        </Grid>
      )
    } else if (details.STATUS_NEW_START_TIME && details.STATUS_NEW_START_TIME === 'approve') {
      return (
        <Grid item>
          <div className={classes.statusActive}>
            <DoneIcon />
            Approved
          </div>
        </Grid>
      )
    } else if (details.STATUS_NEW_START_TIME && details.STATUS_NEW_START_TIME === 'reject') {
      return (
        <Grid item>
          <div className={classes.statusInactive}>
            <CloseIcon />
            Unsuccessful
          </div>
        </Grid>
      )
    } else {
      return (
        <Grid item>
          <div className={classes.statusActive}>
            <DoneIcon />
            Approved
          </div>
        </Grid>
      )
    }
  } else {
    if (details.STATUS_NEW_START_TIME && details.STATUS_NEW_START_TIME === 'req') {
      return (
        <Grid item>
          <div className={classes.statusPanding}>
            {/* <Icon style={{ fontSize: '1.5rem', overflow: 'unset', marginRight: 4 }}>clock</Icon> */}
            <WatchIcon />
            Pending
          </div>
        </Grid>
      )
    } else if (details.STATUS_NEW_START_TIME && details.STATUS_NEW_START_TIME === 'approve') {
      return (
        <Grid item>
          <div className={classes.statusActive}>
            <DoneIcon />
            Approved
          </div>
        </Grid>
      )
    } else if (details.STATUS_NEW_START_TIME && details.STATUS_NEW_START_TIME === 'reject') {
      return (
        <Grid item>
          <div className={classes.statusInactive}>
            <CloseIcon />
            Unsuccessful
          </div>
        </Grid>
      )
    } else {
      return null
    }
  }
}

function DisplayTime({ details }) {
  const startDate = toDateTimeFormat(details.START_TIME)
  const endDate = toDateTimeFormat(details.END_TIME)
  const date = toDateTimeFormat(details.START_TIME)
  const classes = ViewClassStyles()

  if (details.STATUS_NEW_START_TIME) {
    const newDate = toDateTimeFormat(details.NEW_START_TIME)
    if (details.STATUS_NEW_START_TIME && details.STATUS_NEW_START_TIME === 'approve') {
      return (
        <span
          className={classes.displayTimeRoot}
        >{`${startDate.date} • ${startDate.time.hour}:${startDate.time.min} - ${endDate.time.hour}:${endDate.time.min}`}</span>
      )
    } else if (details.STATUS_NEW_START_TIME && details.STATUS_NEW_START_TIME === 'reject') {
      return (
        <Fragment>
          <div
            style={{ fontSize: '13px', lineHeight: '24px' }}
          >{`${startDate.date} • ${startDate.time.hour}:${startDate.time.min} - ${endDate.time.hour}:${endDate.time.min}`}</div>
          {/* <Icon style={{ fontSize: 8, marginLeft: 5, marginRight: 5 }}>arrowForward</Icon> */}
          <div
            className={classes.displayTimeRoot}
            style={{ fontWeight: 600 }}
          >{`${newDate.date} • ${newDate.time.hour}:${newDate.time.min} - ${endDate.time.hour}:${endDate.time.min}`}</div>
        </Fragment>
      )
    } else {
      return (
        <Fragment>
          <span
            style={{ fontSize: '13px', lineHeight: '20px' }}
          >{`${date.date} ${date.time.hour}:${date.time.min}`}</span>
          <Icon style={{ fontSize: 8, marginLeft: 5, marginRight: 5 }}>arrowForward</Icon>
          <span
            className={classes.displayTimeRoot}
          >{`${newDate.date} ${newDate.time.hour}:${newDate.time.min}`}</span>
        </Fragment>
      )
    }
  } else {
    return (
      <span
        className={classes.displayTimeRoot}
      >{`${startDate.date} • ${startDate.time.hour}:${startDate.time.min} - ${endDate.time.hour}:${endDate.time.min}`}</span>
    )
  }
}

function CancelBooking({ detail, handleClose, actions }) {
  const classes = ViewClassStyles()
  const startDate = toDateTimeFormat(detail.START_TIME)
  const endDate = toDateTimeFormat(detail.END_TIME)

  const handleCancel = () => {
    actions
      .CancelBooking({
        orderId: detail.ORDER_ID,
        sessionId: detail.SESSION_ID,
      })
      .then((res) => {
        if (res.status) {
          console.log('handleCancel', res)
          handleClose()
        } else {
          console.error('error handleCancel', res)
          setAlert({
            open: true,
            text: `ไม่สามารถ ยกเลิก ได้`,
          })
        }
      })
  }

  return (
    <Fragment>
      <div style={{ marginBottom: 8 }}>Are you sure you want to cancel the following booking?</div>
      <Grid container className={classes.gridContainer}>
        <Grid item xs={12}>
          <span className={classes.firstText}>Course</span>
        </Grid>
        <Grid item xs={12}>
          <span className={classes.lastText}>{detail.COURSE_DETAIL.DETAIL.COURSE_NAME}</span>
        </Grid>
      </Grid>
      <Grid container className={classes.gridContainer} style={{ marginBottom: 50 }}>
        <Grid item xs={12}>
          <span className={classes.firstText}>Date</span>
        </Grid>
        <Grid item xs={12}>
          <span
            className={classes.lastText}
          >{`${startDate.date} • ${startDate.time.hour}:${startDate.time.min} - ${endDate.time.hour}:${endDate.time.min}`}</span>
        </Grid>
      </Grid>
      <div
        style={{
          position: 'absolute',
          bottom: 0,
          left: '0%',
          width: '100%',
          borderTop: '1px solid #E6E6E6',
        }}
      >
        <Grid container>
          <Grid item xs={6}>
            <div
              style={{
                height: '48px',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                color: '#ED1B24',
                fontWeight: 600,
                fontSize: 14,
                lineHeight: '20px',
                borderBottomLeftRadius: 4,
                cursor: 'pointer',
              }}
              size="small"
              fullWidth
              onClick={handleClose}
            >
              Back
            </div>
          </Grid>
          <Grid item xs={6}>
            <div
              style={{
                height: '48px',
                background: '#ED1B24',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                color: '#FFFFFF',
                fontWeight: 600,
                fontSize: 14,
                lineHeight: '20px',
                borderBottomRightRadius: 4,
                cursor: 'pointer',
              }}
              size="small"
              color="primary"
              fullWidth
              onClick={handleCancel}
            >
              Cancel Booking
            </div>
          </Grid>
        </Grid>
      </div>
    </Fragment>
  )
}

function RequestSubmitted({ detail, dataFromStep, accountsTeacher, accountsStudent, handleClose }) {
  const { profile } = useMember()
  const classes = ViewClassStyles()

  const { items } = dataFromStep
  const endNewDateAddHour = moment(items.NEW_START_TIME).add(items.SESSION_TIME, 's').valueOf()
  const startDate = toDateTimeFormat(detail.START_TIME)
  const endDate = toDateTimeFormat(detail.END_TIME)
  const startNewDate = toDateTimeFormat(items.NEW_START_TIME)
  const endNewDate = toDateTimeFormat(endNewDateAddHour)

  const levelName = detail.COURSE_DETAIL.LEVEL.find((i) => i.LEVEL_ID === detail.LEVEL_ID)
  const teacher = accountsTeacher.find((teach) => teach.ACCOUNT_ID === detail.TEACHER_ID)
  const student = accountsStudent.find((stude) => stude.ACCOUNT_ID === detail.STUDENT_ID)
  // console.log('RequestSubmitted', { dataFromStep, detail })

  return (
    <Fragment>
      <div style={{ textAlign: 'center', marginBottom: 8 }}>
        <Avatar variant="rounded" className={classes.avatarRounded}>
          <AssignmentIcon />
        </Avatar>
        <span
          style={{
            fontSize: 16,
            fontWeight: 600,
            lineHeight: '24px',
            color: '#ED1B24',
          }}
        >
          Request Submitted
        </span>
      </div>
      <div className={classes.subbmit}>
        <span
          style={{
            fontSize: 14,
            lineHeight: '20px',
            color: 'rgba(0, 0, 0, 0.6)',
          }}
        >{`An email regarding your request will be sent to ${profile.EMAIL}`}</span>
      </div>
      <div className={classes.subbmitDescription}>
        <Grid container className={classes.gridContainer} spacing={2}>
          <Grid item xs={4}>
            <span className={classes.firstText}>Course</span>
          </Grid>
          <Grid item xs={8}>
            <span className={classes.lastText}>{detail.COURSE_DETAIL.DETAIL.COURSE_NAME}</span>
          </Grid>
        </Grid>
        <Grid container className={classes.gridContainer} spacing={2}>
          <Grid item xs={4}>
            <span className={classes.firstText}>Level</span>
          </Grid>
          <Grid item xs={8}>
            <span className={classes.lastText}>{levelName?.LEVEL || '-'}</span>
          </Grid>
        </Grid>
        <Grid container className={classes.gridContainer} spacing={2}>
          <Grid item xs={4}>
            <span className={classes.firstText}>Class Duration</span>
          </Grid>
          <Grid item xs={8}>
            <span className={classes.lastText}>{displayTime(detail.SESSION_TIME)}</span>
          </Grid>
        </Grid>
        <Grid container className={classes.gridContainer} spacing={2}>
          <Grid item xs={4}>
            <span className={classes.firstText}>Teacher</span>
          </Grid>
          <Grid item xs={8}>
            <span className={classes.lastText}>
              {teacher ? `${teacher?.FISRT_NAME} ${teacher?.LAST_NAME}` : '-'}
            </span>
          </Grid>
        </Grid>
        <Grid container className={classes.gridContainer} spacing={2}>
          <Grid item xs={4}>
            <span className={classes.firstText}>Student</span>
          </Grid>
          <Grid item xs={8}>
            <span className={classes.lastText}>
              {`${student?.FISRT_NAME} ${student?.LAST_NAME}`}
            </span>
          </Grid>
        </Grid>
        <Grid container className={classes.gridContainer} spacing={2}>
          <Grid item xs={4}>
            <span className={classes.firstText}>Original Date</span>
          </Grid>
          <Grid item xs={8}>
            <span className={classes.lastText}>
              {`${startDate.date} • ${startDate.time.hour}:${startDate.time.min} - ${endDate.time.hour}:${endDate.time.min}`}
            </span>
          </Grid>
        </Grid>
        <Grid container className={classes.gridContainer} spacing={2}>
          <Grid item xs={4}>
            <span className={classes.firstText}>New Date</span>
          </Grid>
          <Grid item xs={8}>
            <span className={classes.lastText}>
              {`${startNewDate.date} • ${startNewDate.time.hour}:${startNewDate.time.min} - ${endNewDate.time.hour}:${endNewDate.time.min}`}
            </span>
          </Grid>
        </Grid>
      </div>
      <div className={classes.buttonRoot}>
        <Button size="large" fullWidth onClick={handleClose}>
          Done
        </Button>
      </div>
    </Fragment>
  )
}

function EditSession({ detail, handleClose, actions }) {
  const [link, setLink] = useState('')
  const [note, setNote] = useState('')

  const classes = ViewClassStyles()
  const startDate = toDateTimeFormat(detail.START_TIME)
  const endDate = toDateTimeFormat(detail.END_TIME)
  const levelName = detail.COURSE_DETAIL.LEVEL.find((i) => i.LEVEL_ID === detail.LEVEL_ID)

  const handleChangLink = ({ target }) => {
    // console.log('handleChangLink', target.value)
    setLink(target.value)
  }
  const handleChangeNote = ({ target }) => {
    setNote(target.value)
  }
  const huandleSave = () => {
    const res = link.replace(/\n/g, ' ').split(' ')
    actions
      .editSession({
        links: res,
        note,
        orderId: detail.ORDER_ID,
        sessionId: detail.SESSION_ID,
      })
      .then((res) => {
        if (res.status) {
          handleClose()
        }
      })
    // console.log('huandleSave', { link, note, res })
  }

  useEffect(() => {
    const LINKS = detail.LINKS
    const NOTE = detail.NOTE
    setLink(LINKS.join('\n'))
    setNote(NOTE)
  }, [])

  return (
    <Fragment>
      <div className={classes.title}>
        <span>{levelName?.LEVEL || '-'}</span>
      </div>
      <div>
        <span className={classes.editSessionDate}>
          {`${startDate.date} • ${startDate.time.hour}:${startDate.time.min} - ${endDate.time.hour}:${endDate.time.min}`}
        </span>
      </div>
      <div className={classes.inputRoot}>
        <Input
          label="Class Description"
          value={note}
          onChange={handleChangeNote}
          multiline
          rows={4}
          outlined
          fullWidth
        />
      </div>
      <div className={classes.inputRoot}>
        <Input
          label="Links"
          value={link}
          onChange={handleChangLink}
          multiline
          rowsMax={4}
          outlined
          fullWidth
        />
      </div>
      <div>
        <Grid container spacing={2}>
          <Grid item xs={4}>
            <Button onClick={handleClose} fullWidth>
              Cancel
            </Button>
          </Grid>
          <Grid item xs>
            <Button color="primary" onClick={huandleSave} fullWidth>
              Save Changes
            </Button>
          </Grid>
        </Grid>
      </div>
    </Fragment>
  )
}

function Reschedule({ detail, accountsTeacher, accountsStudent, onReschedule, actions }) {
  const [alert, setAlert] = useState({
    open: false,
    text: '',
  })
  const [state, setState] = useState({
    date: '',
    time_hour: '',
    time_min: '',
  })

  const onChange = async (value, name) => {
    let data = {}
    if (name === 'date') {
      data = {
        ...state,
        [name]: value,
        time_hour: '',
        time_min: '',
      }
    } else if (name === 'time_hour') {
      data = {
        ...state,
        [name]: value,
      }
    } else if (name === 'time_min') {
      data = {
        ...state,
        [name]: value,
      }
    }
    setState(data)
  }

  const classes = ViewClassStyles()
  const startDate = toDateTimeFormat(detail.START_TIME)
  const endDate = toDateTimeFormat(detail.END_TIME)
  const levelName = detail.COURSE_DETAIL.LEVEL.find((i) => i.LEVEL_ID === detail.LEVEL_ID)
  const teacher = accountsTeacher.find((teach) => teach.ACCOUNT_ID === detail.TEACHER_ID)
  const student = accountsStudent.find((stude) => stude.ACCOUNT_ID === detail.STUDENT_ID)

  const handleResc = async () => {
    await actions
      .rescheduleSession({
        ...state,
        orderId: detail.ORDER_ID,
        sessionId: detail.SESSION_ID,
      })
      .then((res) => {
        if (res.status) {
          console.log('handleResc', res)
          onReschedule({
            page: 4,
            title: 'Request Submitted',
            icon: 'doc',
            items: res.message.data,
          })
        } else {
          console.error('error handleResc', res)
          setAlert({
            open: true,
            text: `ไม่สามารถ Rescedule ${moment(res.data).format('D MMM YYYY HH:mm')} ได้`,
          })
        }
      })
  }

  return (
    <Fragment>
      <div>
        <Grid container className={classes.gridContainer} spacing={2}>
          <Grid item xs={4}>
            <span className={classes.firstText}>Course</span>
          </Grid>
          <Grid item xs={8}>
            <span className={classes.lastText}>{detail.COURSE_DETAIL.DETAIL.COURSE_NAME}</span>
          </Grid>
        </Grid>
        <Grid container className={classes.gridContainer} spacing={2}>
          <Grid item xs={4}>
            <span className={classes.firstText}>Level</span>
          </Grid>
          <Grid item xs={8}>
            <span className={classes.lastText}>{levelName?.LEVEL || '-'}</span>
          </Grid>
        </Grid>
        <Grid container className={classes.gridContainer} spacing={2}>
          <Grid item xs={4}>
            <span className={classes.firstText}>Class Duration</span>
          </Grid>
          <Grid item xs={8}>
            <span className={classes.lastText}>{displayTime(detail.SESSION_TIME)}</span>
          </Grid>
        </Grid>
        <Grid container className={classes.gridContainer} spacing={2}>
          <Grid item xs={4}>
            <span className={classes.firstText}>Teacher</span>
          </Grid>
          <Grid item xs={8}>
            <span className={classes.lastText}>
              {teacher ? `${teacher?.FISRT_NAME} ${teacher?.LAST_NAME}` : '-'}
            </span>
          </Grid>
        </Grid>
        <Grid container className={classes.gridContainer} spacing={2}>
          <Grid item xs={4}>
            <span className={classes.firstText}>Student</span>
          </Grid>
          <Grid item xs={8}>
            <span className={classes.lastText}>
              {`${student?.FISRT_NAME} ${student?.LAST_NAME}`}
            </span>
          </Grid>
        </Grid>
        <Grid container className={classes.gridContainer} spacing={2}>
          <Grid item xs={4}>
            <span className={classes.firstText}>Original Date</span>
          </Grid>
          <Grid item xs={8}>
            <span className={classes.lastText}>
              {`${startDate.date} • ${startDate.time.hour}:${startDate.time.min} - ${endDate.time.hour}:${endDate.time.min}`}
            </span>
          </Grid>
        </Grid>
      </div>
      <div>
        <div className={classes.inputRoot}>
          <span>Please select Date and Time</span>
        </div>
        <div className={classes.inputRoot}>
          <CustomInputDate
            label="Date"
            name="date"
            onChange={onChange}
            value={state.date}
            outlined
            fullWidth
          />
        </div>

        <div className={classes.inputRoot}>
          <div className={classes.boxInputTime}>
            <div className={classes.labelTime}>Class Time</div>
            <Grid container>
              <Grid items xs={6}>
                <CustomInputTimeH
                  label="HH"
                  name="time_hour"
                  onChange={onChange}
                  value={state.time_hour}
                  options={getHours()}
                  disabled={!state.date}
                  outlined
                  fullWidth
                />
              </Grid>
              <Grid items xs={6}>
                <CustomInputTimeM
                  label="MM"
                  name="time_min"
                  onChange={onChange}
                  value={state.time_min}
                  options={getMinutes()}
                  disabled={!state.date}
                  outlined
                  fullWidth
                />
              </Grid>
            </Grid>
          </div>
        </div>

        {/* <div className={classes.inputRoot}>
          <CustomInputTime
            label="Class Time"
            name="class_time"
            onChange={onChange}
            value={state.class_time}
            options={toTimeFormat()}
            disabled={!state.date}
            outlined
            fullWidth
          />
        </div> */}
        <div className={classes.alertRoot}>
          <Collapse in={alert.open}>
            <Alert
              classes={{ filledError: classes.alert, icon: classes.alertIcon }}
              severity={'error'}
              onClose={() => setAlert({ open: false, text: '' })}
            >
              {alert.text}
            </Alert>
          </Collapse>
        </div>
        <div className={classes.buttonRoot}>
          <Button
            color="primary"
            size="large"
            fullWidth
            disabled={!state.time_hour || !state.time_min}
            onClick={handleResc}
          >
            Submit Request
          </Button>
        </div>
      </div>
    </Fragment>
  )
}

function Views({
  detail,
  accountsTeacher,
  accountsStudent,
  type,
  onReschedule,
  actions,
  handleClose,
}) {
  const [disabled, setDisabled] = useState(false)

  const classes = ViewClassStyles()
  const startDate = toDateTimeFormat(detail.START_TIME)
  const endDate = toDateTimeFormat(detail.END_TIME)
  const levelName = detail.COURSE_DETAIL.LEVEL.find((i) => i.LEVEL_ID === detail.LEVEL_ID)
  const teacher = detail.COURSE_DETAIL.ACCOUNTS
    ? detail.COURSE_DETAIL.ACCOUNTS.find((teach) => teach.ACCOUNT_ID === detail.TEACHER_ID)
    : null
  const student = accountsStudent.find((stude) => stude.ACCOUNT_ID === detail.STUDENT_ID)
  // console.log('Views', {
  //   detail,
  //   type,
  //   accountsTeacher,
  //   accountsStudent,
  //   teacher,
  //   student,
  //   levelName,
  // })

  useEffect(() => {
    if (
      !bookandrescheduleflow(
        detail.START_TIME,
        detail.RCREATED_AT,
        detail.COURSE_DETAIL.DETAIL.CREATE_TICKET_TIME,
        detail.COURSE_DETAIL.DETAIL.CREATE_TICKET_TIME_LIMIT,
      )
    ) {
      setDisabled(true)
    } else if (type === 'teacher') {
      setDisabled(true)
    } else if (detail.S_STATUS === 'COMPLETE') {
      setDisabled(true)
    }
  }, [])

  return (
    <Fragment>
      <div className={classes.title}>
        <span>{detail.COURSE_DETAIL.DETAIL.COURSE_NAME}</span>
      </div>
      <div style={{ marginBottom: 12 }}>
        <DisplayTime details={detail} />
      </div>
      <Grid container spacing={2}>
        {type === 'teacher' && (
          <Grid item>
            <div
              style={{ fontWeight: 600, justifyContent: 'center' }}
              className={classes.edit}
              onClick={() =>
                onReschedule({
                  page: 3,
                  title: 'Edit Class',
                  icon: 'edit',
                })
              }
            >
              {/* <Icon style={{ fontSize: '1.5rem', overflow: 'unset', marginRight: 4 }}>edit</Icon> */}
              <span>Edit Class</span>
            </div>
          </Grid>
        )}
        {type !== 'teacher' && <StatusComponent details={detail} />}
        {type === 'teacher' ? null : detail.S_STATUS === 'WAITING_APPROVE' ? (
          <Grid item>
            <div
              style={{
                pointerEvents: disabled ? 'none' : 'all',
                color: '#ED1B24',
                justifyContent: 'center',
                fontWeight: 600,
              }}
              className={classes.reschedule}
              onClick={() =>
                onReschedule({
                  page: 5,
                  title: 'Cancel Booking',
                  icon: 'messageSquare',
                })
              }
            >
              {/* <Icon style={{ fontSize: '1.5rem', overflow: 'unset', marginRight: 4 }}>clock</Icon> */}
              <span>Cancel Booking</span>
            </div>
          </Grid>
        ) : detail.STATUS_NEW_START_TIME && detail.STATUS_NEW_START_TIME === 'req' ? (
          <Grid item>
            <div
              style={{
                pointerEvents: disabled ? 'none' : 'all',
                color: '#ED1B24',
                justifyContent: 'center',
                fontWeight: 600,
              }}
              className={classes.reschedule}
              onClick={() =>
                actions
                  .CancelRequest({
                    orderId: detail.ORDER_ID,
                    sessionId: detail.SESSION_ID,
                  })
                  .then((res) => {
                    if (res.status) {
                      console.log('CancelRequest', res)
                      handleClose()
                    }
                  })
              }
            >
              {/* <Icon style={{ fontSize: '1.5rem', overflow: 'unset', marginRight: 4 }}>clock</Icon> */}
              <span>Cancel Request</span>
            </div>
          </Grid>
        ) : (
          <Grid item>
            <div
              style={{
                pointerEvents: disabled ? 'none' : 'all',
                color: disabled && '#CCCCCC',
                justifyContent: 'center',
                fontWeight: 600,
              }}
              className={classes.reschedule}
              onClick={() =>
                onReschedule({
                  page: 2,
                  title: 'Request reschedule',
                  icon: 'messageSquare',
                })
              }
            >
              {/* <Icon style={{ fontSize: '1.5rem', overflow: 'unset', marginRight: 4 }}>clock</Icon> */}
              <span>Reschedule Request</span>
            </div>
          </Grid>
        )}
      </Grid>

      <div style={{ paddingTop: 26 }}>
        {/* <Grid container className={classes.gridContainer}>
          <Grid item xs={3}>
            <span className={classes.firstText}>Time</span>
          </Grid>
          <Grid item xs={9}>
            <span className={classes.lastText}>
              {`${startDate.date} • ${startDate.time.hour}:${startDate.time.min} - ${endDate.time.hour}:${endDate.time.min}`}
            </span>
          </Grid>
        </Grid> */}
        {/* <Grid container className={classes.gridContainer}>
          <Grid item xs={3}>
            <span className={classes.firstText}>Level</span>
          </Grid>
          <Grid item xs={9}>
            <span className={classes.lastText}>{levelName?.LEVEL || '-'}</span>
          </Grid>
        </Grid> */}
        <Grid container className={classes.gridContainer}>
          <Grid item xs={3}>
            <span className={classes.firstText}>Teacher</span>
          </Grid>
          <Grid item xs={9}>
            <span className={classes.lastText}>
              {teacher ? `${teacher?.FISRT_NAME} ${teacher?.LAST_NAME}` : '-'}
            </span>
          </Grid>
        </Grid>
        <Grid container className={classes.gridContainer}>
          <Grid item xs={3}>
            <span className={classes.firstText}>Student</span>
          </Grid>
          <Grid item xs={9}>
            <span className={classes.lastText}>
              {`${student?.FISRT_NAME} ${student?.LAST_NAME}`}
            </span>
          </Grid>
        </Grid>
        <Grid container className={classes.gridContainer}>
          <Grid item xs={3}>
            <span className={classes.firstText}>Note</span>
          </Grid>
          <Grid item xs={9}>
            <span className={classes.lastText}>{detail.NOTE}</span>
          </Grid>
        </Grid>
        <Grid container className={classes.gridContainer}>
          <Grid item xs={3}>
            <span className={classes.firstText}>Links</span>
          </Grid>
          <Grid item xs={9}>
            {detail.LINKS.map((link, key) => (
              <Grid key={key} style={{ paddingBottom: 8, wordWrap: 'break-word' }} item xs={12}>
                <a href={link} target="_blank" className={classes.links}>
                  <span className={classes.lastText}>{link}</span>
                </a>
              </Grid>
            ))}
          </Grid>
        </Grid>
      </div>
    </Fragment>
  )
}

function ViewClass({
  viewSession,
  accountsTeacher,
  accountsStudent,
  type,
  modalOpen,
  actions,
  modalActions,
}) {
  const [step, setStep] = useState({
    page: 1,
    title: 'Session Details',
    icon: 'doc',
  })

  const handleClose = () => {
    modalActions.setViewClassOpenModal(false)
    setStep({
      page: 1,
      title: 'Session Details',
      icon: 'doc',
    })
  }
  const classes = ViewClassStyles()

  return (
    <Modal
      open={modalOpen}
      onClose={handleClose}
      headerChildren={
        step.page !== 4 && (
          <IconRounded
            title={step.title}
            icon={
              <Icon style={{ fontSize: '1.5rem', overflow: 'unset', width: 17 }}>{step.icon}</Icon>
            }
          />
        )
      }
    >
      <div className={classes.root}>
        {step.page === 1 && (
          <Views
            detail={viewSession}
            accountsTeacher={accountsTeacher}
            accountsStudent={accountsStudent}
            type={type}
            onReschedule={setStep}
            actions={actions}
            handleClose={handleClose}
          />
        )}
        {step.page === 2 && (
          <Reschedule
            detail={viewSession}
            accountsTeacher={accountsTeacher}
            accountsStudent={accountsStudent}
            type={type}
            actions={actions}
            onReschedule={setStep}
          />
        )}
        {step.page === 3 && (
          <EditSession
            detail={viewSession}
            type={type}
            handleClose={handleClose}
            actions={actions}
          />
        )}
        {step.page === 4 && (
          <RequestSubmitted
            detail={viewSession}
            accountsTeacher={accountsTeacher}
            accountsStudent={accountsStudent}
            type={type}
            handleClose={handleClose}
            dataFromStep={step}
          />
        )}
        {step.page === 5 && (
          <CancelBooking
            detail={viewSession}
            accountsTeacher={accountsTeacher}
            accountsStudent={accountsStudent}
            type={type}
            handleClose={handleClose}
            dataFromStep={step}
            actions={actions}
          />
        )}
      </div>
    </Modal>
  )
}

const mapStateToProps = (state) => {
  return {
    viewSession: state.myCourse.viewSession,
    modalOpen: state.modal.isViewClassOpen,
    accountsTeacher: state.myCourse.accountsTeacher,
    accountsStudent: state.myCourse.accountsStudent,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
    modalActions: bindActionCreators(ModalActions, dispatch),
  }
}

const enhancer = compose(nextConnect(mapStateToProps, mapDispatchToProps))

export default enhancer(ViewClass)
