import React, { Fragment } from 'react'
import { getStudentUser, getAdminUser } from '@config/constants'

import ScheduleDetailsStyles from './ScheduleDetailsStyles'

function StatusComponent({ details }) {
  const classes = ScheduleDetailsStyles()

  if (details.S_STATUS === 'WAITING_APPROVE') {
    return <span className={classes.statusPanding}>Pending</span>
  } else {
    if (details.STATUS_NEW_START_TIME && details.STATUS_NEW_START_TIME === 'req') {
      return <span className={classes.statusPanding}>Pending</span>
    } else if (details.STATUS_NEW_START_TIME && details.STATUS_NEW_START_TIME === 'approve') {
      return <span className={classes.statusRescheduled}>Rescheduled</span>
    } else if (details.STATUS_NEW_START_TIME && details.STATUS_NEW_START_TIME === 'reject') {
      return <span className={classes.statusInactive}>Unsuccessful</span>
    } else {
      return null
    }
  }
}

function ScheduleDetails({ details, actions, profile }) {
  const classes = ScheduleDetailsStyles()
  // console.log('ScheduleDetails', details)
  const handleViewSession = (orderId, sessionId) => {
    // console.log('handleViewSession', { orderId, sessionId })
    actions.viewSession(orderId, sessionId)
  }
  // console.log('ScheduleDetails', details)
  const sortDetails = details && details.sort((a, b) => a.START_TIME - b.START_TIME)

  return (
    <div className={classes.rootScheduleDetails}>
      {sortDetails && sortDetails.length > 0 ? (
        sortDetails.map((item, index) => (
          <div
            key={index}
            className={classes.sectionScheduleDetails}
            onClick={() => handleViewSession(item.ORDER_ID, item.SESSION_ID)}
          >
            <div className="time">{`${item.START_TIME_FORMATE.time.hour}:${item.START_TIME_FORMATE.time.min}`}</div>
            <div className={classes.sectionDetails}>
              <div>
                {getAdminUser(profile.TYPE) ? null : <StatusComponent details={item} />}
                <span className="title">{item.MY_COUSE.COURSE_DETAIL.DETAIL.COURSE_NAME}</span>
              </div>
              <div className="description">{item.NOTE}</div>
            </div>
          </div>
        ))
      ) : (
        <div className={classes.noEventRoot}>
          <div>No Event</div>
        </div>
      )}
    </div>
  )
}

export default ScheduleDetails
