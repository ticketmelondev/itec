import { makeStyles, withStyles } from '@material-ui/core/styles'
import MuiExpansionPanel from '@material-ui/core/ExpansionPanel'
import MuiExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import MuiExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'

export const ExpansionPanel = withStyles({
  root: {
    border: '1px solid rgba(0, 0, 0, .125)',
    boxShadow: 'none',
    '&:not(:last-child)': {
      borderBottom: 0,
    },
    '&:before': {
      display: 'none',
    },
    '&$expanded': {
      margin: 'auto',
    },
  },
  expanded: {},
})(MuiExpansionPanel)

export const ExpansionPanelSummary = withStyles({
  root: {
    // backgroundColor: 'rgba(0, 0, 0, .03)',
    borderBottom: '1px solid rgba(0, 0, 0, .125)',
    marginBottom: -1,
    minHeight: 56,
    '&$expanded': {
      minHeight: 56,
    },
  },
  content: {
    overflow: 'hidden',
    '&$expanded': {
      margin: '12px 0',
    },
  },
  expanded: {},
})(MuiExpansionPanelSummary)

export const ExpansionPanelDetails = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiExpansionPanelDetails)

const BodySectionStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    // background: '#FFFFFF',
    // boxShadow: '0px 0px 4px rgba(0, 0, 0, 0.25)',
    // padding: ' 30px 23px',
  },
  titleRoot: {
    marginBottom: 20,
  },
  title: {
    color: '#000000',
    lineHeight: '29px',
    borderLeft: `4px solid ${theme.palette.primary.main}`,
    paddingLeft: '10px',
  },
  detailsRoot: {
    marginTop: 12,
  },
  details: {
    color: '#7A7A7A',
    lineHeight: '163.2%',
    fontWeight: 300,
  },
  sectionRoot: {
    // marginTop: 20,
    '& .title': {
      fontWeight: '600',
      fontSize: '18px',
      lineHeight: '28px',
      color: '#4D4D4D',
      marginBottom: 12,
    },
    '& .sub-title': {
      fontSize: '13px',
      lineHeight: '20px',
      color: '#808080',
    },
  },
  sectionCard: {
    position: 'relative',
    width: '100%',
    height: 159,
    boxShadow: '0px 0px 4px rgba(0, 0, 0, 0.2)',
    borderRadius: '5px',
  },
  sectionBody: {
    height: '100%',
    maxHeight: '100px',
    padding: '12px 20px',
    '& .title': {
      fontWeight: 600,
      fontSize: '16px',
      lineHeight: '24px',
      color: 'rgba(0, 0, 0, 0.8)',
    },
    '& .details': {
      fontSize: '14px',
      lineHeight: '20px',
      color: '#7A7A7A',
    },
  },
  sectionBottom: {
    padding: '12px 20px',
    background: '#F8F7F7',
    position: 'absolute',
    bottom: 0,
    width: '100%',
    height: 61,
    display: 'flex',
    alignItems: 'center',
    '& .duration': {
      fontSize: '14px',
      lineHeight: '20px',
      color: '#7A7A7A',
      flexGrow: 1,
    },
  },
  lineClamp: {
    overflow: 'hidden',
    display: '-webkit-box',
    '-webkit-line-clamp': 2,
    '-webkit-box-orient': 'vertical',
  },
  captionRoot: {
    marginTop: 5,
  },
  caption: {
    fontWeight: 300,
    fontSize: '14px',
    lineHeight: '163.2%',
    color: '#7A7A7A',
  },
  buttonRoot: {
    [theme.breakpoints.down('xs')]: {
      minWidth: 138,
    },
  },
  typographyLevel: {
    fontWeight: 600,
    fontSize: '16px',
    lineHeight: '24px',
    color: 'rgba(0, 0, 0, 0.8)',
  },
  typographyLevelSecond: {
    fontSize: 13,
    lineHeight: '20px',
    /* identical to box height, or 154% */
    color: '#7A7A7A',
  },
}))

export default BodySectionStyles
