import React from 'react'
import { bindActionCreators } from 'redux'
import { nextConnect } from '@redux/store'
import * as modalActions from '@actions/modalActions'
import * as bookingActions from '@actions/bookingActions'

import Container from '@material-ui/core/Container'

import Breadcrumb from '@components/Breadcrumb'

import CourseOverviewStyled from './CourseOverviewStyled'
import Header from './Header'
import BodySection from './BodySection'
import OtherCourse from './OtherCourse'

function CourseOverview({
  courseDetails,
  isLoading,
  orderIsLoading,
  modalActions,
  bookingActions,
}) {
  const { DETAIL, LEVEL } = courseDetails
  const classes = CourseOverviewStyled()
  const uniqueAge = [...new Set(LEVEL.map((a) => a.AGE))]
  const uniqueLevels = LEVEL.filter(
    ((set) => (f) => !set.has(f.LEVEL) && set.add(f.LEVEL))(new Set()),
  ).filter((i) => !i.HIDE)

  // console.log('CourseOverview', isLoading)
  return (
    <div style={{ backgroundColor: '#F8F7F7', paddingBottom: '56px' }}>
      {isLoading ? null : (
        <Container>
          <div style={{ marginBottom: 48, paddingTop: 30 }}>
            <Header
              details={DETAIL}
              levels={uniqueLevels}
              ages={uniqueAge}
              bookingActions={bookingActions}
              modalActions={modalActions}
              items={LEVEL}
            />
          </div>
          <div style={{ marginBottom: 56 }}>
            <BodySection details={DETAIL} levels={uniqueLevels} orderIsLoading={orderIsLoading} />
          </div>
          <div style={{ marginBottom: 56 }}>
            <span style={{ color: '#808080' }}>
              * Course level might change according to level test which you will receive after
              booking the course
            </span>
          </div>
        </Container>
      )}
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    courseDetails: state.courses.course,
    isLoading: state.isLoading.courseLoading,
    orderIsLoading: state.isLoading.orderLoading,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    modalActions: bindActionCreators(modalActions, dispatch),
    bookingActions: bindActionCreators(bookingActions, dispatch),
  }
}

export default nextConnect(mapStateToProps, mapDispatchToProps)(CourseOverview)
