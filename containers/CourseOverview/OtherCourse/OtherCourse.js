import React from 'react'
import Container from '@material-ui/core/Container'
import GridList from '@material-ui/core/GridList'
import GridListTile from '@material-ui/core/GridListTile'
import Typography from '@material-ui/core/Typography'

import Card from '@components/Card'

import OtherCourseStyles from './OtherCourseStyled'

function OtherCourse({ items }) {
  const classes = OtherCourseStyles()

  return (
    <Container>
      <div className={classes.titleRoot}>
        <Typography className={classes.title} variant="h5">
          Other Courses
        </Typography>
      </div>

      <div className={classes.root}>
        <GridList className={classes.gridList} cols={3.4}>
          {items.map((item, key) => (
            <GridListTile key={key} className={classes.gridListTile}>
              <div style={{ width: 268 }}>
                <Card
                  className={classes.cardBgColor}
                  contentClassName={classes.cardContent}
                  bgColor="#F6F6F6"
                  title={item.name}
                  detail="หลักสูตรภาษาอังกฤษทั่วไปวัตถุประสงค์เพื่อพัฒนา ทักษะการสื่อสารและความเข้าใจ"
                  page="course"
                  slugId={item.id}
                />
              </div>
            </GridListTile>
          ))}
        </GridList>
      </div>
    </Container>
  )
}

export default OtherCourse
