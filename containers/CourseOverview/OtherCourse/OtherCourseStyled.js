import { makeStyles } from '@material-ui/core/styles'

const OtherCourseStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: '#F6F6F6',
  },
  gridList: {
    flexWrap: 'nowrap',
    // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
    transform: 'translateZ(0)',
  },
  gridListTile: {
    height: '283px!important',
  },
  title: {
    color: theme.palette.primary.light,
  },
  titleBar: {
    background:
      'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
  },
  cardBgColor: {
    backgroundColor: 'inherit',
    boxShadow: 'none',
    borderRadius: 0,
  },
  titleRoot: {
    marginBottom: 23,
  },
  title: {
    fontSize: '24px',
    lineHeight: '28px',
    color: '#333333',
  },
  cardContent: {
    paddingLeft: 0,
  },
}))

export default OtherCourseStyles
